%calculates the time of comparison
%file part of the source code to perform the quantification method
%----------------------------------------------------------------
%     Copyright (C) 2018 Ana Victoria Ponce Bobadilla
%     email: anavictoria.ponce@iwr.uni-heidelberg.de
%
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.
clc
clear all
close all

%code 2 defined the time 2 compare the areas againt the WT group

invitro_parameters;

time4comparison=zeros(1,5);%there are only 5 other groups


wound_name=strcat(dir4experimental_data,'percentage_wound_area_evolution.mat');
load(wound_name);

% calculate the time the area becomes 0 and divided by two 
time_wound_close=zeros(num_scratches,1);

for s=1:num_scratches
   timee= find(normalized_wound_area(s,:)==0);
   if isempty(timee)
       time_wound_close(s)=48; %48/2
   else
       time_wound_close(s)=min(timee);
   end
   
end


lastime_area_groups=zeros(num_samples,num_groups);

for group=1:num_groups
    rate_vec=[];
    
    for scratch=1+num_samples*(group-1):group*num_samples
        
        rate_vec=[rate_vec; time_wound_close(scratch)];
        
    end
    lastime_area_groups(:,group)= rate_vec;
    
end

min_wt=min(lastime_area_groups(:,2));

for i=1:length(non_wt2group)

   time2compare= min(min_wt,min(lastime_area_groups(:,non_wt2group(i))));
    time2compare=floor(time2compare/2);
    
time4comparison(i)=time2compare;
end



name=strcat(dir4experimental_data,'time4comparison','.mat');
save(name,'time4comparison');