%save optimal windwo velocties
%file part of the source code to perform the quantification method
%----------------------------------------------------------------
%     Copyright (C) 2018 Ana Victoria Ponce Bobadilla
%     email: anavictoria.ponce@iwr.uni-heidelberg.de
%
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.
clear
clc
close all

invitro_parameters
%output from this script, the optimal window velocities

windowed_optimal_vels_cols=[];%for doing the statistical tests
windowed_optimal_vels=[];%for plotting the boxplot
individual_vel_index=[];
group_optimal_vels_cols=[];


%setting the optimal window size and the num of win cells 
file_fit_results=strcat(dir4experimental_data,'fit_results.mat');
load(file_fit_results);

%this is to determine the num_win_cells
window_size=optimal_window;
windowed_subdomain=window_size:window_size:sizeofdomain;
windowed_subdomain=[ 0 windowed_subdomain];
%add the residual domain that is left if the soze of the domain is not a multiple of the window size
if(windowed_subdomain(end)~=sizeofdomain)
    windowed_subdomain=[windowed_subdomain sizeofdomain ];
end

num_win_cells= length(windowed_subdomain)-1;%total of cells with the window size


%WT group will be group S1
s=5;
folder4_exp_s=strcat(dir4experimental_data,num2str(s,'%g'),'/');
file_windowed_velocities=strcat(folder4_exp_s,'windowed_velocities.mat');
    load(file_windowed_velocities);
   
windowed_subdomain4scratch5=window_size:window_size:sizeofdomain4scratch5;
windowed_subdomain4scratch5=[ 0 windowed_subdomain4scratch5];
%add the residual domain that is left if the soze of the domain is not a multiple of the window size
if(windowed_subdomain4scratch5(end)~=sizeofdomain4scratch5)
    windowed_subdomain4scratch5=[windowed_subdomain4scratch5 sizeofdomain4scratch5 ];
end

num_win_cells4scratch5= length(windowed_subdomain4scratch5)-1;%total of cells with the window size
j=1;

   vels=[vel_l_w(window_size,1:num_win_cells4scratch5) vel_r_w(window_size,1:num_win_cells4scratch5)];
    windowed_optimal_vels=[windowed_optimal_vels;  vels'];
    individual_vel_index=[individual_vel_index ; j*ones(num_win_cells4scratch5*2,1)];
group_1_optimal_vels_cols=vels';

j=2;


for s=6:8
    
    folder4_exp_s=strcat(dir4experimental_data,num2str(s,'%g'),'/');
    file_windowed_velocities=strcat(folder4_exp_s,'windowed_velocities.mat');
    load(file_windowed_velocities);
    
    vels=[vel_l_w(window_size,1:num_win_cells) vel_r_w(window_size,1:num_win_cells)];
    windowed_optimal_vels=[windowed_optimal_vels;  vels'];
     
     group_1_optimal_vels_cols=[group_1_optimal_vels_cols; vels'];
    individual_vel_index=[individual_vel_index; j*ones(num_win_cells*2,1) ];
    
    j=j+1;
    
end

%for adding the other group info 

for s=1:4
    
    folder4_exp_s=strcat(dir4experimental_data,num2str(s,'%g'),'/');
    file_windowed_velocities=strcat(folder4_exp_s,'windowed_velocities.mat');
    load(file_windowed_velocities);
    
    vels=[vel_l_w(window_size,1:num_win_cells) vel_r_w(window_size,1:num_win_cells)];
    windowed_optimal_vels=[windowed_optimal_vels; vels'];
     windowed_optimal_vels_cols=[windowed_optimal_vels_cols  vels'];
    individual_vel_index=[individual_vel_index ; j*ones(num_win_cells*2,1)];
    
    j=j+1;
    
end

for s=9:24
    
    folder4_exp_s=strcat(dir4experimental_data,num2str(s,'%g'),'/');
    file_windowed_velocities=strcat(folder4_exp_s,'windowed_velocities.mat');
    load(file_windowed_velocities);
    
    vels=[vel_l_w(window_size,1:num_win_cells) vel_r_w(window_size,1:num_win_cells)];
    windowed_optimal_vels=[windowed_optimal_vels; vels'];
     windowed_optimal_vels_cols=[windowed_optimal_vels_cols  vels'];
    individual_vel_index=[individual_vel_index ; j*ones(num_win_cells*2,1)];
    
    j=j+1;
    
end


 win_vel_groups=zeros(num_samples*2*num_win_cells,num_groups-1);
group_vel_index=[];
%zeros(num_samples*2*num_win_cells*(num_groups-1)+(num_samples-1)*2*num_win_cells+num_win_cells4scratch5*2,1);
group_vel_index=[group_vel_index;1*ones((num_samples-1)*2*num_win_cells+num_win_cells4scratch5*2,1)];

 
j=2;

%  windowed_optimal_vels_cols
for group=1:num_groups-1

    velocity_vec_per_group=[ ];

    for scratch=1+num_samples*(group-1):group*num_samples   
velocity_vec_per_group=[velocity_vec_per_group; windowed_optimal_vels_cols(:,scratch)]; 
    end
    group_vel_index=[group_vel_index; j*ones(num_samples*2*num_win_cells,1)];

    j=j+1;
     win_vel_groups(:,group)=velocity_vec_per_group;

end


filename2save_optimal_velocities=strcat(dir4experimental_data,'optimal_velocities.mat');
save(filename2save_optimal_velocities,'individual_vel_index','windowed_optimal_vels','num_win_cells','num_win_cells4scratch5','group_vel_index','win_vel_groups','group_1_optimal_vels_cols');

    
    
    