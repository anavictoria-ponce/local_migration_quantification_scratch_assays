%code that calculates the percentage area
%file part of the source code of the manuscript "Local migration quantification method for scratch assays"
%----------------------------------------------------------------
%     Copyright (C) 2018 Ana Victoria Ponce Bobadilla
%     email: anavictoria.ponce@iwr.uni-heidelberg.de
%
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.
%--------------------------------------------------
clc
clear all
close all

in_silico_parameters
for j=1:length(pm_values)
  for k=1:length(pp_values)
   % j=5;
    %k=1;
    folder4pm_pp=strcat('pm_',num2str(pm_values(j),'%g'),'_pp_',num2str(pp_values(k),'%g'),'/')

    for rep=1:repetitions

     % rep=1;
      file_in=strcat(dir_up_global,folder4pm_pp,'area_evolution_',num2str(rep,'%g'),'.mat');
      load(file_in);

      percentage_wound_area=zeros(1,total_num_images);

      initial_area=individual_wound_area(1);
      for f=1:total_num_images
        percentage_wound_area(f)=100*individual_wound_area(f)/initial_area;

      end


      file_out=strcat(dir_up_global,folder4pm_pp,'percentage_wound_area_evolution_',num2str(rep,'%g'),'.mat');
      save(file_out,'percentage_wound_area');
      %  clearvars cells_area holes_area

    end
  end
end
