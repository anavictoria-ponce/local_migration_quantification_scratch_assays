%code that detects the contour for the in silico data
%file part of the source code of the manuscript "Local migration quantification method for scratch assays"
%----------------------------------------------------------------
%     Copyright (C) 2018 Ana Victoria Ponce Bobadilla
%     email: anavictoria.ponce@iwr.uni-heidelberg.de
%
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.
clc
clear all


in_silico_parameters

for j=1:length(pm_values)
    for k=1:length(pp_values)
%j=1;
%k=2;
folder4pm_pp=strcat('pm_',num2str(pm_values(j),'%g'),'_pp_',num2str(pp_values(k),'%g'),'/')
        for rep=1:repetitions

%rep=1;



file_in=strcat(dir_up_global,folder4pm_pp,'evolution_pm_',num2str(pm_values(j),'%g'),'_pp_',num2str(pp_values(k),'%g'),'_',num2str(rep,'%g'),'.mat');

file_out=strcat(dir_up_global,folder4pm_pp,'contour_evolution_pm_',num2str(pm_values(j),'%g'),'_pp_',num2str(pp_values(k),'%g'),'_',num2str(rep,'%g'),'.mat');


load(file_in);

contour_l_evolution=[];
contour_r_evolution=[];

for f=1:total_num_images+1

    labels_out2=evolution{1,f}|evolution{2,f};
    %labels_out2=squeeze(evolution{1}(f,:,:)|evolution{2}(f,:,:));
    labels=bwlabel(labels_out2);% we need bwlabel for using regionprops


    reg=regionprops(labels);


    % to plot the labels
    %imagesc(labels)

    %creates a vector of the areas so we can identify the max
    areas = [reg.Area];
    % This is to identify the two largest areas (which are the cell masses)
    index1=find(areas==max(areas));

    if  length(index1)>1
        tmp=index1;
        index1=tmp(1);
        index2=tmp(2);
    else

        index2=find(areas==max(areas(areas~=max(areas))));
    end


    if  (reg(index1).BoundingBox(1)<2) && (reg(index1).BoundingBox(3)>498)
        break;
    end

    % we consider the bounding box so we can identify the left and the right
    % masses
    if (reg(index1).BoundingBox(1)<50) && (reg(index1).BoundingBox(2)<50)
        index_l=index1;
        index_r=index2;
    else
        index_l=index2;
        index_r=index1;
    end % index_l is for the left mass and simlarwise for the right

    %we defined the two cell front according to the index of the largest areas
    im_l=ismember(labels, index_l);
    im_r=ismember(labels, index_r);


    %filling the two regions so we only take into account the border
    im_l(:,1)=1;
    im_l(:,2)=1;
    max_u=max(find(im_l(2,:)));
    im_l(1,1:max_u)=1;
    im_l(2,1:max_u)=1;
    max_u=max(find(im_l(end-1,:)));
    im_l(end,1:max_u)=1;
    im_l(end-1,1:max_u)=1;

    im_r(:,end)=1;
    im_r(:,end-1)=1;
    min_u=min(find(im_r(2,:)));
    im_r(1,min_u:end)=1;
    im_r(2,min_u:end)=1;

    min_u=min(find(im_r(end-1,:)));
    im_r(end,min_u:end)=1;
    im_r(end-1,min_u:end)=1;

    im_lmorph = bwmorph(im_l,'diag');
    im_lmorph = bwmorph(im_lmorph,'majority');
    im_lmorph = bwmorph(im_lmorph,'close');

    im_rmorph = bwmorph(im_r,'diag');
    im_rmorph = bwmorph(im_rmorph,'majority');
    im_rmorph = bwmorph(im_rmorph,'close');

    % filling the holes
    im_l=imfill(im_l,'holes');
    im_r=imfill(im_r,'holes');


    im_l = bwareaopen(im_l, 50);
    im_r = bwareaopen(im_r, 50);

    max_u=max(find(im_l(3,:)));
    im_l(1,1:max_u)=1;
    im_l(2,1:max_u)=1;
    max_u=max(find(im_l(end-2,:)));
    im_l(end,1:max_u)=1;
    im_l(end-1,1:max_u)=1;

    min_u=min(find(im_r(3,:)));
    im_l = bwareaopen(im_l, 10);
    im_r(1,min_u:end)=1;
    im_r(2,min_u:end)=1;

    min_u=min(find(im_r(end-2,:)));
    im_r(end,min_u:end)=1;
    im_r(end-1,min_u:end)=1;


    %we added this to detect later when calculating velocities which is
%the end of the visible contour
im_l(:,1)=1;
im_r(:,end)=1;

    %to identify the interface ussing the diff method
    interface_l=zeros(size(im_l,2),1);
    interface_r=zeros(size(im_l,2),1);

    for iter=1:size(im_l,2)
        interface_l(iter)=max(find(diff(im_l(iter,:))~=0));%this is to consider the furthest pixel from the left
        interface_r(iter)=min(find(diff(im_r(iter,:))~=0));%this is to consider the furthest pixel form the right
    end

    contour_l_evolution=[contour_l_evolution  interface_l];
    contour_r_evolution=[contour_r_evolution  interface_r];

end

%          contour_evolutions(1,simulation_num)=contour_l_evolution;
%         contour_evolutions(2,simulation_num)={contour_r_evolution};
% name=strcat(work_dir,'contour_data_dimensionalized','.mat');
save(file_out,'contour_l_evolution','contour_r_evolution');
clearvars contour_l_evolution contour_r_evolution
        end
   end
end

disp('Finished detecting contour');
