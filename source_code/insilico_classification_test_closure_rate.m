%does the classification test with respect to the closure rate method
%file part of the source code of the manuscript "Local migration quantification method for scratch assays"
%----------------------------------------------------------------
%     Copyright (C) 2018 Ana Victoria Ponce Bobadilla
%     email: anavictoria.ponce@iwr.uni-heidelberg.de
%
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.
clc
clear all
close all

in_silico_parameters


for q=1:length(pm_test_size)
    for r=1:length(pp_test_size)

   %  q=1;
    %           r=1;
        kstst_result_closure_tests=zeros(length(pm_values),length(pp_values),num_tests);
        ttst_result_closure_tests=zeros(length(pm_values),length(pp_values),num_tests);

        string4test_subset=strcat('pm_test',num2str(q,'%g'),'_pp_test_',num2str(r,'%g'),'_');


        jchosen=pm_test_size(q);
        kchosen=pp_test_size(r);
        folder4pm_pp_fixed=strcat('pm_',num2str(pm_values(jchosen),'%g'),'_pp_',num2str(pp_values(kchosen),'%g'),'/')
        name=strcat(dir_up_global,folder4pm_pp_fixed,'closure_rate_samples.mat');
        load(name);
        rate_closure_fixed=rate_closure;

        for i=1:num_tests
          %  i
            %i=1;
            subset4chosen = randperm(repetitions,num_samples);
            subset4against= randperm(repetitions,num_samples);
            data2compare_chosen=rate_closure_fixed(subset4chosen);

            for j=1:length(pm_values)
                for k=1:length(pp_values)
                    %   j=2;
                    %   k=2;

                    folder4pm_pp=strcat('pm_',num2str(pm_values(j),'%g'),'_pp_',num2str(pp_values(k),'%g'),'/');
                    name=strcat(dir_up_global,folder4pm_pp,'closure_rate_samples.mat');
                    load(name);
                    data2compare_against=rate_closure(subset4against);
                    kstst_result_closure_tests(j,k,i)=kstest2(data2compare_chosen,data2compare_against);
                    ttst_result_closure_tests(j,k,i)=ttest2(data2compare_chosen,data2compare_against);
                end % end of loop for pp values
            end%end of loop for pm values

        end

         kstst_result_closure_mean=100*sum(kstst_result_closure_tests,3)/num_tests;
         ttst_result_closure_mean=100*sum(ttst_result_closure_tests,3)/num_tests;

        filename=strcat(dir_up_global,dir4resultsclassification_test,string4test_subset,'class_test_closure_rate');
        save(filename,'kstst_result_closure_tests','ttst_result_closure_tests','kstst_result_closure_mean','ttst_result_closure_mean');
    end
end
