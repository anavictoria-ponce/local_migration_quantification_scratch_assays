%calculates the closure rate
%file part of the source code to perform the quantification method
%----------------------------------------------------------------
%     Copyright (C) 2018 Ana Victoria Ponce Bobadilla
%     email: anavictoria.ponce@iwr.uni-heidelberg.de
%
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.
clc
clear all
close all

%code 2 defined the time 2 compare the areas againt the WT group

invitro_parameters;


name=strcat(dir4experimental_data,'wound_area_evolution.mat');
load(name);
rate_closure=zeros(num_scratches,1);

for s=1:num_scratches

     folder4_exp_s=strcat(dir4experimental_data,num2str(s,'%g'),'/');
        file4contour_evolution=strcat( folder4_exp_s,'contour_evolution.mat');
        load(file4contour_evolution);

        total_num_images=size(contour_l_evolution,2);%total number of images
        x=0:total_num_images-1;
        x=0.5*x;

     p_l=polyfit(x, wound_area(s,1:total_num_images),1);
     rate_closure(s)=abs(p_l(1))/(2*sizeofdomain);



end
%this is to fix  the rate closure of scratch 5 since it does not cover all
%the field view

s=5;
    folder4_exp_s=strcat(dir4experimental_data,num2str(s,'%g'),'/');
    file4contour_evolution=strcat( folder4_exp_s,'contour_evolution.mat');
    load(file4contour_evolution);

   sizeofdomain_l=min(find(contour_l_evolution(:,1)<2))-1;
    sizeofdomain_r=min(find(contour_r_evolution(:,1)>498))-1;
    sizeofdomain=min(sizeofdomain_l,sizeofdomain_r);
rate_closure(s)=abs(p_l(1))/(2*sizeofdomain);


rate_closure_groups=zeros(num_samples,num_groups);

for group=1:num_groups
    rate_vec=[];

    for scratch=1+num_samples*(group-1):group*num_samples

        rate_vec=[rate_vec; rate_closure(scratch)];

    end
    rate_closure_groups(:,group)= rate_vec;

end
temp=rate_closure_groups(:,2);
rate_closure_groups(:,2)=rate_closure_groups(:,1);
rate_closure_groups(:,1)=temp;

name=strcat(dir4experimental_data,'closure_rate_samples.mat');
save(name,'rate_closure_groups');
