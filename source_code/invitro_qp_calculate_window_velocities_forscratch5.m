    %code to calculate the window velocties for the scratch 5

    %file part of the source code to perform the quantification method
    %----------------------------------------------------------------
    %     Copyright (C) 2018 Ana Victoria Ponce Bobadilla
    %     email: anavictoria.ponce@iwr.uni-heidelberg.de
    %
    %     This program is free software: you can redistribute it and/or modify
    %     it under the terms of the GNU General Public License as published by
    %     the Free Software Foundation, either version 3 of the License, or
    %     (at your option) any later version.
    %
    %     This program is distributed in the hope that it will be useful,
    %     but WITHOUT ANY WARRANTY; without even the implied warranty of
    %     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    %     GNU General Public License for more details.
    %
    %     You should have received a copy of the GNU General Public License
    %     along with this program.  If not, see <https://www.gnu.org/licenses/>.

    clc
    clear all
    close all

    invitro_parameters

    s=5;
    folder4_exp_s=strcat(dir4experimental_data,num2str(s,'%g'),'/');
    file4contour_evolution=strcat( folder4_exp_s,'contour_evolution.mat');
    load(file4contour_evolution);


    total_num_images=size(contour_l_evolution,2);%total number of images
    x=0:total_num_images-1;
    x=0.5*x;

    %vectors to save the windowed velocity, xintercept and residual from left and right front
    vel_l_w=zeros(length(window_sizes),sizeofdomain);
    vel_r_w=zeros(length(window_sizes),sizeofdomain);
    xintercept_l_w=zeros(length(window_sizes),sizeofdomain);
    xintercept_r_w=zeros(length(window_sizes),sizeofdomain);
    resid_l_w=zeros(length(window_sizes),sizeofdomain);
    resid_r_w=zeros(length(window_sizes),sizeofdomain);

    sizeofdomain_l=min(find(contour_l_evolution(:,1)<2))-1;
    sizeofdomain_r=min(find(contour_r_evolution(:,1)>498))-1;
    sizeofdomain=min(sizeofdomain_l,sizeofdomain_r);
    window_sizes=1:1:sizeofdomain;

for window_size=1:length(window_sizes)
    %defining the windowed subdomains
    windowed_subdomain=window_size:window_size:sizeofdomain;
    windowed_subdomain=[ 0 windowed_subdomain];

    if(windowed_subdomain(end)~=sizeofdomain)%add the residual domain that is left if the soze of the domain is not a multiple of the window size
        windowed_subdomain=[windowed_subdomain sizeofdomain ];
    end

    num_win_cells= length(windowed_subdomain)-1;%total of cells with the window size

    %piecewise contour with respect to the windwo size
    piecewise_contour_l_evolution=zeros(num_win_cells,total_num_images);
     piecewise_contour_r_evolution=zeros(num_win_cells,total_num_images);
    for i=1:num_win_cells% for the number of subdomains
        for time_p=1:total_num_images
            init=windowed_subdomain(i)+1;
            endd=windowed_subdomain(i+1);
            piecewise_contour_l_evolution(i,time_p)=mean(contour_l_evolution(init:endd,time_p));
             piecewise_contour_r_evolution(i,time_p)=mean(contour_r_evolution(init:endd,time_p));

        end
        %fitting of the piecewise_contour_evolution
        p_l=polyfit(x,piecewise_contour_l_evolution(i,1:end),1);
        vel_l_w(window_size,i)=p_l(1);
        xintercept_l_w(window_size,i)=p_l(2);

         p_r=polyfit(x,sizeofdomain-piecewise_contour_r_evolution(i,1:end),1);
        vel_r_w(window_size,i)=p_r(1);
        xintercept_r_w(window_size,i)=p_r(2);
    end
end%end of the loop window size

sizeofdomain4scratch5=sizeofdomain;





file_windowed_velocities=strcat(folder4_exp_s,'windowed_velocities.mat');
save(file_windowed_velocities,'vel_l_w','vel_r_w','xintercept_l_w','xintercept_r_w','sizeofdomain4scratch5');
%clearvars vel_l_w vel_r_w xintercept_l_w xintercept_r_w
