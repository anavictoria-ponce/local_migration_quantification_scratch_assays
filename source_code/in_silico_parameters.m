%in silico parameters
%file part of the source code to perform the quantification method
%----------------------------------------------------------------
%     Copyright (C) 2018 Ana Victoria Ponce Bobadilla
%     email: anavictoria.ponce@iwr.uni-heidelberg.de
%
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.

sizeofartificialdomain=50;
sizeofdomain=500;
repetitions=150;

total_num_images=48;
dir_up_global='../insilico_data/';
dir4resultsclassification_test='classification_tests/';
dir4comparing_robustness='../simulations_parameter_exploration/comparing_stat_robustness_data/';
pm_values=0:0.1:1;
pp_values=0:0.01:0.1;
window_sizes=1:1:sizeofdomain;
dir4plots='../plotsfrom_simulations_parameter_exploration/';
dir4processing='~/Dropbox/PHD_Heidelberg/Project/files_from_HD/';
%parameters4comparing_robustness
num_tests=20;%20;
num_samples=4;%3:1:8;
pm_test_size=2:4:11;
pp_test_size=2:4:11;
