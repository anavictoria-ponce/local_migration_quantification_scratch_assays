%performs the segmentation algorithm
%file part of the source code to perform the quantification method
%----------------------------------------------------------------
%     Copyright (C) 2018 Ana Victoria Ponce Bobadilla
%     email: anavictoria.ponce@iwr.uni-heidelberg.de
%
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.
clear
clc
close all

invitro_parameters
addpath files4growcut_segmentation_algorithm



filename2save_num=strcat(dir4experimental_data,'number_pictures_4scratch.mat');
load(filename2save_num);%loads the number of pictures 4 scratch % the number of images that were actually on the tiff %number_pictures_4scratch

%this file will save the numbers of scratches for which it was possible
%to make the segmentation
filename2save_num_aftersegmentation=strcat(dir4experimental_data,'number_pictures_4scratch_aftersegmentation.mat');
number_pictures_4scratch_aftersegmentation=zeros(num_scratches,1);

for s=1:num_scratches
    s
    num_images=number_pictures_4scratch(s);
    folder_out=strcat(dir4experimental_data,num2str(s,'%g'),'/');
    for k = 1:num_images
        
       
        %k
        %k =1;
        file4_unsegmented_img=strcat(folder_out, filename4_unsegmentedscratch,num2str(k,'%g'),'.png');
        I=imread( file4_unsegmented_img);
        
        %resizing the image since 1 pixel =2 mu m
        I=imresize(I,0.5);
        
        %defining the labels for the growcut algorithm
        BW1 = edge(I,'canny');
        BW2 = edge(I,'roberts');
        BW3=BW2 | BW1;
        labels=BW3;
        labels=double(labels);
        
        %calculating the std deviation of the image density
        std_img=movingstd2(I,2);
        
        %seting the region where the image intensity does not change much
        BW4=std_img<std_deviation_threshold;
        BW4=imopen((BW4),strel('disk',round(18/sampling_factor),0));
        BW4=imclose((BW4),strel('disk',round(18/sampling_factor),0));
        labels(BW4)=-1;
        
        if (sum(sum(BW4))==0)
            %  msgbox('there is no pixels that satisfy std_img<500, no background pixels, growcut wont work');%this message does not work when matlab is run from the terminal
            break; % we need to include the break when the code does not detect any pixels that fulfill <std_deviation_threshold, usually happens when the wound is closed
        end
         %saving the number of scratches after segmentation
        number_pictures_4scratch_aftersegmentation(s)=k;
%         
%         [detected_contour, strengths] = growcut(uint8(std_img),double(labels));
%         
%         %saving the segmentated_img4detecting the contour later
%         filename_4detectedcontour=strcat(folder_out,'detected_contour_',num2str(k,'%g'),'.mat');
%         save(filename_4detectedcontour,'detected_contour');
%         
%         %plotting the segmented_img
%         f = figure('visible','off');
%      
%         cont2=imcontour(detected_contour);
%         imagesc(I)
%         set(gca,'YDir','normal')
%         hold on
%         plot(cont2(1,:),cont2(2,:),'b.','markersize',5)
%         colormap gray
%         hold off
%         set(gca,'xtick',[],'ytick',[])
%         filename_4img_detectedcontour=strcat(folder_out,'segmented_scratch_',num2str(k,'%g'),'.jpg');
%         saveas(f, filename_4img_detectedcontour);
        
    end
end

save(filename2save_num_aftersegmentation,'number_pictures_4scratch_aftersegmentation')
