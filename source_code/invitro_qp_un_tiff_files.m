%untiff the files
%file part of the source code to perform the quantification method
%----------------------------------------------------------------
%     Copyright (C) 2018 Ana Victoria Ponce Bobadilla
%     email: anavictoria.ponce@iwr.uni-heidelberg.de
%
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.
clear
clc
close all

invitro_parameters


    number_pictures_4scratch=zeros(num_scratches,1);

    for s=1:num_scratches
        s
        %s=1;
          file_in=strcat(folder_4scratch,'wound_healing_images.tif');

        %this is to obtain the number of images for each scratch
        info = imfinfo(file_in);
        number_pictures_4scratch(s)=numel(info);
        num_images=number_pictures_4scratch(s);

        for k = 1:num_images
            %k
            file_out=strcat(folder_4scratch, filename4_unsegmentedscratch,num2str(k,'%g'),'.png');
            tmp = imread(file_in, k);
            A(:,:,k)=(tmp(1:sampling_factor:end,1:sampling_factor:end));
            imwrite(A(:,:,k),file_out)
        end
        
        
    end

    filename2save_num=strcat(dir4experimental_data,'number_pictures_4scratch.mat');
    save(  filename2save_num,'number_pictures_4scratch')
    
