%calculates the area evolution
%file part of the source code to perform the quantification method
%----------------------------------------------------------------
%     Copyright (C) 2018 Ana Victoria Ponce Bobadilla
%     email: anavictoria.ponce@iwr.uni-heidelberg.de
%
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.
%code to create the area info object that has the cell area and the holes area
%--------------------------------------------------
clc
clear all
close all

invitro_parameters


filename2save_num_aftersegmentation=strcat(dir4experimental_data,'number_pictures_4scratch_aftersegmentation.mat');

load(filename2save_num_aftersegmentation);%loads the number of pictures 4 scratch % the number of images that were actually on the tiff %number_pictures_4scratch
total_num_images=48;
wound_area=zeros(num_scratches,total_num_images);

for s=1:num_scratches
  s
  %   s=1;
  individual_wound_area=zeros(1,total_num_images);
  cells_area=zeros(2,total_num_images);
  holes_area=zeros(2,total_num_images);
  folder_out=strcat(dir4experimental_data,num2str(s,'%g'),'/');

  total_num_images_aftersegmentation=number_pictures_4scratch_aftersegmentation(s);

  for k=1:total_num_images_aftersegmentation
    %    k=1;
    filename_4detectedcontour=strcat(folder_out,'detected_contour_',num2str(k,'%g'),'.mat');
    load(filename_4detectedcontour);
    labels=bwlabel(detected_contour);% we need bwlabel for using regionprops
    reg=regionprops(labels);

    %creates a vector of the areas so we can identify the max
    areas = [reg.Area];


    % This is to identify the two largest areas (which are the cell masses)
    index1=find(areas==max(areas));


    if  length(index1)>1%in the case there are two regions with the largest area value
      tmp=index1;
      index1=tmp(1);
      index2=tmp(2);
    else

      index2=find(areas==max(areas(areas~=max(areas))));
    end

    %to see if the monolayers have already touched
    if  (reg(index1).BoundingBox(1)<2) && (reg(index1).BoundingBox(3)>((sizeofdomain)-2))
      %this is the case where the monolayers have touched


      mass=ismember(labels, index1);

      max_u=max(find(mass(3,:)));
      mass(1,1:max_u)=1;
      mass(2,1:max_u)=1;
      max_u=max(find(mass(end-3,:)));
      mass(end,1:max_u)=1;
      mass(end-1,1:max_u)=1;
      mass(:,1)=1;


      min_u=min(find(mass(3,:)));
      mass(1,min_u:end)=1;
      mass(2,min_u:end)=1;

      min_u=min(find(mass(end-3,:)));
      mass(end,min_u:end)=1;
      mass(end-1,min_u:end)=1;
      mass(:,end)=1;

      mass = bwmorph(mass,'diag');
      mass = bwmorph(mass,'majority');
      mass = bwmorph(mass,'close');



      reg_mass=regionprops(mass);
      individual_wound_area(k)=sizeofdomain*sizeofdomain-reg_mass(1).Area;

    else %this is the case that the two monolayers havent touched


      % we consider the bounding box so we can identify the left and the right
      % masses
      if (reg(index1).BoundingBox(1)<100) && (reg(index1).BoundingBox(2)<100)
        index_l=index1;
        index_r=index2;
      else
        index_l=index2;
        index_r=index1;
      end % index_l is for the left mass and simlarwise for the right



      %we defined the two cell front according to the index of the largest areas
      im_l=ismember(labels, index_l);
      im_r=ismember(labels, index_r);

      %filling the two regions so we only take into account the border

      max_u=max(find(im_l(3,:)));
      im_l(1,1:max_u)=1;
      im_l(2,1:max_u)=1;
      max_u=max(find(im_l(end-3,:)));
      im_l(end,1:max_u)=1;
      im_l(end-1,1:max_u)=1;
      im_l(:,1)=1;

      min_u=min(find(im_r(3,:)));
      im_r(1,min_u:end)=1;
      im_r(2,min_u:end)=1;

      min_u=min(find(im_r(end-3,:)));
      im_r(end,min_u:end)=1;
      im_r(end-1,min_u:end)=1;
      im_r(:,end)=1;

      im_lmorph = bwmorph(im_l,'diag');
      im_lmorph = bwmorph(im_lmorph,'majority');
      im_lmorph = bwmorph(im_lmorph,'close');

      im_rmorph = bwmorph(im_r,'diag');
      im_rmorph = bwmorph(im_rmorph,'majority');
      im_rmorph = bwmorph(im_rmorph,'close');
      %---------------------------------saving the cell area-------------------------
      reg_lmorph=regionprops(im_lmorph);
      cells_area(1,k)=reg_lmorph.Area;

      reg_rmorph=regionprops(im_rmorph);
      cells_area(2,k)=reg_rmorph.Area;

      %--------------------------------------calculating the area of holes in the left interface
      num_holes=1-bweuler(im_lmorph);

      if (num_holes==0)%%if there are holes

        holes_area(1,k)=0;

      else

        labels_holes=bwlabel(~im_lmorph);
        reg_holes=regionprops(labels_holes);
        num_holes=size(reg_holes,1)-1;
        for i=1:num_holes
          holes_area(1,k)=holes_area(1,k)+reg_holes(1+i).Area;
        end

      end

      %--------------------------------------calculating the area of holes in the right interface
      num_holes=1-bweuler(im_rmorph);

      if (num_holes==0)%%if there are holes

        holes_area(2,k)=0;

      else

        labels_holes=bwlabel(~im_rmorph);
        reg_holes=regionprops(labels_holes);
        num_holes=size(reg_holes,1)-1;
        for i=1:num_holes
          holes_area(2,k)=holes_area(2,k)+reg_holes(1+i).Area;
        end
      end

      individual_wound_area(k)=sizeofdomain*sizeofdomain-sum(cells_area(:,k))-sum(holes_area(:,k));

    end

  end%end of total_images

  if (total_num_images<48)

    individual_wound_area(total_num_images+1:48)=0;

  end

  wound_area(s,:)=individual_wound_area;

end

name=strcat(dir4experimental_data,'wound_area_evolution.mat');
save(name,'wound_area');
