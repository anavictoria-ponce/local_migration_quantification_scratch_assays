/*%preprocess the output data from the abm simulations
%file part of the source code of the manuscript "Local migration quantification method for scratch assays"
%----------------------------------------------------------------
%     Copyright (C) 2018 Ana Victoria Ponce Bobadilla
%     email: anavictoria.ponce@iwr.uni-heidelberg.de
%
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.*/
#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iomanip>      // std::setprecision
#include <set>
#include <array>
#include <limits>
#include <sstream>
#include <string>

class ABM_processing_data
{

public:

ABM_processing_data(int _num_repetitions );

void pre_processing_netlogo_simulation_data();
private:
int num_repetitions;
double pm, pp;
const double delta_pm, delta_pp;
const double pm_start_value, pp_start_value, pm_end_value,  pp_end_value;
std::string output_folder_name, dir_up;
const  int pm_values_length, pp_values_length;
std::vector <double> pm_values, pp_values;
double current_pm, current_pp;
const unsigned int  sizeofartificialdomain;




};

ABM_processing_data::ABM_processing_data(int _num_repetitions):
num_repetitions(_num_repetitions),
delta_pm(0.1),
delta_pp(0.01),
pm_start_value(0),
pp_start_value(0),
pm_end_value(1),
pp_end_value(0.1),
pm_values_length(int((pm_end_value-pm_start_value)/delta_pm)+1),
pp_values_length(int((pp_end_value-pp_start_value)/delta_pp)+1),
dir_up("../insilico_data"),
sizeofartificialdomain(50)
{


pm_values.resize(pm_values_length);

std::cout << "pm values "  << std::endl;
    for (int i=0; i<pm_values_length; i++)
    {
      pm_values[i]=i*delta_pm+pm_start_value;
std::cout << pm_values[i]<< std::endl;
    }

std::cout << "pp values "  << std::endl;
  pp_values.resize(pp_values_length);
    for (int i=0; i<pp_values_length; i++)
    {
      pp_values[i]=i*delta_pp+pp_start_value;
std::cout << pp_values[i]<< std::endl;
    }

}

void ABM_processing_data::pre_processing_netlogo_simulation_data()
{

  //-------------------------------------------------------------------------------------------------

  //strings and chart to be able to output the string of the simulatiosn in the desired output
  std::string temp_filename, infilename;
  char buffer[150]="";


  //for the left and right interface
  std::vector<std::string> sides(2);
  sides[0]="_l.dat";
  sides[1]="_r.dat";

  std::ifstream infile;
  std::string outfile_name;
  std::ofstream outfile;

  std::vector<std::vector<unsigned int> > domain_occupancy(sizeofartificialdomain, std::vector<unsigned int>(sizeofartificialdomain,0));
  std::vector<std::vector<unsigned int> > domain_occupancy_empty(sizeofartificialdomain, std::vector<unsigned int>(sizeofartificialdomain,0));
  unsigned int duration_length(0);
  unsigned int xcor, ycor;


  for (int i=0; i<pm_values_length; i++)
  {
  for (int j=0; j<pp_values_length; j++)
    {



      //updating the current pm and pp values
      current_pm=pm_values[i];
      current_pp=pp_values[j];
std::cout << "pm: " << current_pm << " pp: " << current_pp << std::endl;
      //setting the simulations folder
      snprintf(buffer,150,"/pm_%g_pp_%g/",current_pm,current_pp);
      temp_filename=buffer;
      output_folder_name=dir_up+temp_filename;
//std::cout << output_folder_name << std::endl;
  //loop to read the netlogo simulations from the  pm, pp and repetitions

      for (int rep=1; rep<num_repetitions+1; rep++)
      {

        //to create the formatted string based on the pm and pp values
        snprintf(buffer,150,"pm_%g_pp_%g_%d",current_pm,current_pp,rep);
        temp_filename=buffer;

        for (unsigned int num_sides=0; num_sides<sides.size(); num_sides++)
        {

          infilename=output_folder_name+temp_filename+sides[num_sides];//filename of the simulations
          infile.open(infilename,std::ifstream::in);
        //  std::cout << "infilename " << infilename << std::endl;

          while (infile >> xcor >> ycor)
          {
            if (ycor==9000)
            {
//std::cout << "xcor: " << xcor << std::endl;
              if(xcor>0)//this if forces to the code start when time_step=1 that is when the domain_occupnacy is filled with the positions from time_step 1
              {
                duration_length=(xcor-1)/5;//the duration is updated every time the ycor=9000
                outfile.close();
                outfile_name=output_folder_name+"occupancy_"+temp_filename+"_numimg_"+std::to_string((xcor-1)/5)+sides[num_sides];
                outfile.open (outfile_name);


                //storing the output of the domain_occupancy in the outfile in a matrix format
                for (unsigned int i=0;i<sizeofartificialdomain; i++)
                {
                  for (unsigned int j=0;j<sizeofartificialdomain; j++)
                  {
                    outfile << " " << domain_occupancy[i][j];
                  }
                  outfile << "\n";
                }
              }

              //setting the domain_occupancy to an empty occupation
              domain_occupancy=domain_occupancy_empty;

            }
            else
            {
              //    saves the dmain_occupancy for the x and y cor
              domain_occupancy[ycor][xcor]=1;//the x and y cor need to be reversed so the image is uploaded correctly in matlab


            }


          }//end of the while infile

          outfile.close();
          outfile_name=output_folder_name+"occupancy_"+temp_filename+"_numimg_"+std::to_string(duration_length+1)+sides[num_sides];//we use duration_length+1 to consider the last time step
          outfile.open (outfile_name);
          //actually duration_length+1 shoudl always be 48
      //    std::cout <<  outfile_name << std::endl;
          //storing the output of the domain_occupancy in the outfile in a matrix format
          for (unsigned int i=0;i<sizeofartificialdomain; i++)
          {
            for (unsigned int j=0;j<sizeofartificialdomain; j++)
            {
              outfile << " " << domain_occupancy[i][j];
            }
            outfile << "\n";
          }
          infile.close();
          domain_occupancy=domain_occupancy_empty;//to reinitate the domain occupancy

        }//end sides loop





      }//end rep


}
}

}

int main ()
{

ABM_processing_data abm_pd(150);
abm_pd.pre_processing_netlogo_simulation_data();



return 0;
}
