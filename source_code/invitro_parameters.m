%contains the in_vitro parameters
%file part of the source code to perform the quantification method
%----------------------------------------------------------------
%     Copyright (C) 2018 Ana Victoria Ponce Bobadilla
%     email: anavictoria.ponce@iwr.uni-heidelberg.de
%
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.

num_scratches=24;
filename4_unsegmentedscratch = 'unsegmented_scratch_';
filename4_segmentedscratch = 'segmented_scratch_';

sizeofdomain=500;
dir4experimental_data='~/workspace/scratch_assay_ABM/wound_healing_jazmin_new_analysis/';
dir4extraplots='extra_plots/';
window_sizes=1:1:sizeofdomain;
sampling_factor=1;
std_deviation_threshold=500;

total_num_images=48;

num_groups=6;
num_samples=4;%num samples for each group

%setting the color for the different groups
colors4plot=[];
colors4eachgroup=[ [0    0    0];
    [1    0    0];
    [0.9843    0.7961    0.2118];
    [0.75    0    0.75];
    [0.4660    0.6740    0.1880];
    [0.3010    0.7450    0.9330];
    [0.6350    0.0780    0.1840]];

%to get colors for the different groups
for i=1:num_groups
    for j=1:num_samples
        colors4plot=[colors4plot;colors4eachgroup(i,:)];
    end
end

non_wt2group=[1,3:6];
