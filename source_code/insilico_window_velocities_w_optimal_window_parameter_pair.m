%performs the sensitivity analysis of the velocities from the abm
%file part of the source code of the manuscript "Local migration quantification method for scratch assays"
%----------------------------------------------------------------
%     Copyright (C) 2018 Ana Victoria Ponce Bobadilla
%     email: anavictoria.ponce@iwr.uni-heidelberg.de
%
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.
clc
clear all
close all

in_silico_parameters

mean_window_velocities=zeros(length(pm_values),length(pp_values));
std_window_velocities=zeros(length(pm_values),length(pp_values));
for j=1:length(pm_values)
    for k=1:length(pp_values)
        %j=1;
        %k=2;
        folder4pm_pp=strcat('pm_',num2str(pm_values(j),'%g'),'_pp_',num2str(pp_values(k),'%g'),'/')
        file_fit_results=strcat(dir_up_global,folder4pm_pp,'fit_results.mat');
        load(file_fit_results);

        window_size=optimal_window;
        windowed_subdomain=window_size:window_size:sizeofdomain;
        windowed_subdomain=[ 0 windowed_subdomain];
        %add the residual domain that is left if the soze of the domain is not a multiple of the window size
        if(windowed_subdomain(end)~=sizeofdomain)
            windowed_subdomain=[windowed_subdomain sizeofdomain ];
        end

        num_win_cells= length(windowed_subdomain)-1;%total of cells with the window size

        windowed_optimal_vels=[];

        for rep=1:num_samples

            file_in=strcat(dir_up_global,folder4pm_pp,'windowed_velocities_rep',num2str(rep,'%g'),'.mat');
            load(file_in);

            vels=[vel_l_w(window_size,1:num_win_cells) vel_r_w(window_size,1:num_win_cells)];
            windowed_optimal_vels=[windowed_optimal_vels; vels'];



        end
        mean_window_velocities(j,k)=mean(windowed_optimal_vels);
        std_window_velocities(j,k)=std(windowed_optimal_vels);
    end

end


filename=strcat(dir_up_global,'sensitivity_analysis_mean_var');
save(filename,'mean_window_velocities','std_window_velocities');
