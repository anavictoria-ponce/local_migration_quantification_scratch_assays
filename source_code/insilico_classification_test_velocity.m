%does the classification test with respect to the velocity method
%file part of the source code of the manuscript "Local migration quantification method for scratch assays"
%----------------------------------------------------------------
%     Copyright (C) 2018 Ana Victoria Ponce Bobadilla
%     email: anavictoria.ponce@iwr.uni-heidelberg.de
%
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.
clc
clear all
close all

in_silico_parameters


for q=1:length(pm_test_size)
    for r=1:length(pp_test_size)

%        q=1;
%        r=1;
        kstst_result_vel_tests=zeros(length(pm_values),length(pp_values),num_tests);
        ttst_result_vel_tests=zeros(length(pm_values),length(pp_values),num_tests);

        string4test_subset=strcat('pm_test',num2str(q,'%g'),'_pp_test_',num2str(r,'%g'),'_');


        jchosen=pm_test_size(q);
        kchosen=pp_test_size(r);

        folder4pm_pp_fixed=strcat('pm_',num2str(pm_values(jchosen),'%g'),'_pp_',num2str(pp_values(kchosen),'%g'),'/')


        for i=1:num_tests
            %  i
            %i=1;
            subset4chosen = randperm(repetitions,num_samples);
            subset4against= randperm(repetitions,num_samples);



            for j=1:length(pm_values)
                for k=1:length(pp_values)
  %                  j=5;
  %                  k=1;

                                %defining the optimal window with respect
                                %to the scratches being compared
 fit_results_global=zeros(length(window_sizes),3);
 for rep=1:repetitions
% rep=1;
file_fit=strcat(dir_up_global,folder4pm_pp_fixed,'fit_results_rep',num2str(subset4chosen(rep),'%g'),'.mat');
        load(file_fit);

        fit_results_global(:,1)=fit_results_global(:,1)+sum(fit_squares_l_w,2)+sum(fit_squares_r_w,2);
        fit_results_global(:,2)=fit_results_global(:,2)+sum(fit_r2_l_w,2)+sum(fit_r2_r_w,2);
        fit_results_global(:,3)=fit_results_global(:,3)+fit_lr_distvel_ksdistance_w;



            file_fit=strcat(dir_up_global,folder4pm_pp,'fit_results_rep',num2str(subset4against(rep),'%g'),'.mat');
            load(file_fit);


        fit_results_global(:,1)=fit_results_global(:,1)+sum(fit_squares_l_w,2)+sum(fit_squares_r_w,2);
        fit_results_global(:,2)=fit_results_global(:,2)+sum(fit_r2_l_w,2)+sum(fit_r2_r_w,2);
        fit_results_global(:,3)=fit_results_global(:,3)+fit_lr_distvel_ksdistance_w;


 end

 num_scratches_analyzed=2*num_samples;
fit_results_global(:,1)=fit_results_global(:,1)/(2*sizeofdomain*num_scratches_analyzed);
fit_results_global(:,2)=fit_results_global(:,2)/(2*sizeofdomain*num_scratches_analyzed);
fit_results_global(:,3)=fit_results_global(:,3)/num_scratches_analyzed;

 %to normalized the  residuals results
if (max(fit_results_global(:,1))-min(fit_results_global(:,1))>0)
    fit_resid_normalized=(max(fit_results_global(:,1))-fit_results_global(:,1))/(max(fit_results_global(:,1))-min(fit_results_global(:,1)));
else
    fit_resid_normalized=fit_results_global(:,1);
end

if (max(fit_results_global(:,2))-min(fit_results_global(:,2))>0)
    fit_r2_normalized=(fit_results_global(:,2)-min(fit_results_global(:,2)))/(max(fit_results_global(:,2))-min(fit_results_global(:,2)));
else
    fit_r2_normalized=fit_results_global(:,2);
end


%to normalize the lr velocity distribution distnace
if (max(fit_results_global(:,3))-min(fit_results_global(:,3))>0)
    fit_lr_normalized=(fit_results_global(:,3)-max(fit_results_global(:,3)))/(min(fit_results_global(:,3))-max(fit_results_global(:,3)));
else
    fit_lr_normalized=fit_results_global(:,3);
end

global_fit=1/3*(fit_resid_normalized+fit_lr_normalized+fit_r2_normalized);
max_index=find(max(global_fit)==global_fit);
optimal_window=max_index(1);

window_size=optimal_window;
windowed_subdomain=window_size:window_size:sizeofdomain;
windowed_subdomain=[ 0 windowed_subdomain];
%add the residual domain that is left if the soze of the domain is not a multiple of the window size
if(windowed_subdomain(end)~=sizeofdomain)
    windowed_subdomain=[windowed_subdomain sizeofdomain ];
end

num_win_cells= length(windowed_subdomain)-1;%total of cells with the window size

windowed_optimal_vels=[];
windowed_optimal_vels2=[];
  for rep=1:num_samples

    file_in=strcat(dir_up_global,folder4pm_pp_fixed,'windowed_velocities_rep',num2str(subset4chosen(rep),'%g'),'.mat');
   load(file_in);

    vels=[vel_l_w(window_size,1:num_win_cells) vel_r_w(window_size,1:num_win_cells)];
    windowed_optimal_vels=[windowed_optimal_vels; vels'];

     file_in=strcat(dir_up_global,folder4pm_pp,'windowed_velocities_rep',num2str(subset4against(rep),'%g'),'.mat');
   load(file_in);

    vels=[vel_l_w(window_size,1:num_win_cells) vel_r_w(window_size,1:num_win_cells)];
    windowed_optimal_vels2=[windowed_optimal_vels2; vels'];

end

 data2compare_chosen=windowed_optimal_vels;
 data2compare_against=windowed_optimal_vels2;



                     kstst_result_vel_tests(j,k,i)=kstest2(data2compare_chosen,data2compare_against);
                     ttst_result_vel_tests(j,k,i)=ttest2(data2compare_chosen,data2compare_against);
                 end % end of loop for pp values
             end%end of loop for pm values

         end

         kstst_result_vel_mean=100*sum(kstst_result_vel_tests,3)/num_tests;
         ttst_result_vel_mean=100*sum(ttst_result_vel_tests,3)/num_tests;
        filename=strcat(dir_up_global,dir4resultsclassification_test,string4test_subset,'class_test_vel');
         save(filename,'kstst_result_vel_tests','ttst_result_vel_tests','kstst_result_vel_mean','ttst_result_vel_mean');
     end
end
