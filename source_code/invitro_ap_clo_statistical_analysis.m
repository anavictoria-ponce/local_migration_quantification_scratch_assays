%calculates the statistical analysis
%file part of the statistical analysis to perform the quantification method
%----------------------------------------------------------------
%     Copyright (C) 2018 Ana Victoria Ponce Bobadilla
%     email: anavictoria.ponce@iwr.uni-heidelberg.de
%
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.
clc
clear all
close all

%code 2 defined the time 2 compare the areas againt the WT group

invitro_parameters;
name=strcat(dir4experimental_data,'closure_rate_samples.mat');
load(name);


normality_test_wt=zeros(num_groups,1);
for i=1:num_groups
      data=rate_closure_groups(:,i);
S=(data-mean(data))/sqrt(var(data));
normality_test_wt(i)=kstest(S);
end

%stat test

h_t=zeros(num_groups-1,1);
h_ks=zeros(num_groups-1,1);
h_r=zeros(num_groups-1,1);
p_t=zeros(num_groups-1,1);
p_ks=zeros(num_groups-1,1);
p_r=zeros(num_groups-1,1);

for i=1:num_groups-1
   [h_t(i),p_t(i)]=ttest2(rate_closure_groups(:,1),rate_closure_groups(:,i+1));
   [h_ks(i),p_ks(i)]=kstest2(rate_closure_groups(:,1),rate_closure_groups(:,i+1));
     [p_r(i),h_r(i)]=ranksum(rate_closure_groups(:,1),rate_closure_groups(:,i+1));
end

 hyp2plot=h_t;
         pval2plot=p_t;
%saving results in a table


 hyp2plot=h_t;
         pval2plot=p_t;
%saving results in a table
       fprintf('\\begin{table}[H]\n')
        fprintf(' \\begin{center} \n')
        fprintf(' \\scalebox{0.8}{ \n')
 fprintf(' \\begin{tabular}{|   C{2.25cm} |  C{2.25cm}  |  C{2.25cm} |  C{2.25cm} | C{2.25cm} |  C{2.25cm} | C{2.25cm}|  }	\\hline  \n')
 fprintf(' & S2 & S3 & S4 & S5& S6 \\\\  \\hline \n ')
 fprintf('t-test & ')
 for i=1:4
     fprintf('h=%d, p=%1.3e & ',hyp2plot(i),pval2plot(i))
 end
 fprintf('h=%d, p=%1.3e \\\\  \\hline \n ',hyp2plot(5),pval2plot(5))

         hyp2plot=h_r;
           pval2plot=p_r;
 fprintf('  Wilcoxon rank sum test & ')
 for i=1:4
     fprintf('h=%d, p=%1.3e & ',hyp2plot(i),pval2plot(i))
 end
 fprintf('h=%d, p=%1.3e \\\\  \\hline \n ',hyp2plot(5),pval2plot(5))
 
 
    hyp2plot=h_ks;
         pval2plot=p_ks;
 

 fprintf('ks-test & ')
 for i=1:4
     fprintf('h=%d, p=%1.3e & ',hyp2plot(i),pval2plot(i))
 end
 fprintf('h=%d, p=%1.3e \\\\  \\hline \n ',hyp2plot(5),pval2plot(5))
 fprintf(' \\end{tabular} } \n')
 fprintf(' \\end{center}\\caption{Closure rate \\label{tab:ks_test}} \n')
 fprintf(' \\end{table} \n')
 


 
