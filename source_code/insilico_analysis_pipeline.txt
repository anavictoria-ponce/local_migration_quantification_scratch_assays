%pipeline of how the files need to be called to perform the insilico
%file part of the source code to perform the quantification method
%----------------------------------------------------------------
%     Copyright (C) 2018 Ana Victoria Ponce Bobadilla
%     email: anavictoria.ponce@iwr.uni-heidelberg.de
%
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.

%In matlab
insilico_set_ideal_condition4abm
%In c++
ABM_exploration.cc
ABM_processing_data.cc
%In matlab
insilico_import_data
insilico_qp_segmentation_algorithm
insilico_qp_detecting_contour
insilico_qp_calculate_window_velocities
insilico_qp_calculate_fit_results

insilico_qp_calculate_fit_results
insilico_qp_calculate_optimal_window
insilico_qp_statistical_analysis

insilico_ap_calculate_area_evolution
insilico_ap_per_normalize_area_evolution
insilico_ap_calculate_end_times

insilico_ap_clo_calculate_closure_rate

insilico_window_velocities_w_optimal_window_parameter_pair
insilico_optimal_window_parameter_pair
insilico_classification_test_area
insilico_classification_test_closure_rate
insilico_classification_test_velocity

insilico_sensitivity_analysis_pm
