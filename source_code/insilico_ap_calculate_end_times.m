%code that tracks the evolution of the area
%file part of the source code of the manuscript "Local migration quantification method for scratch assays"
%----------------------------------------------------------------
%     Copyright (C) 2018 Ana Victoria Ponce Bobadilla
%     email: anavictoria.ponce@iwr.uni-heidelberg.de
%
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.
%--------------------------------------------------
clc
clear all
close all

in_silico_parameters
for j=1:length(pm_values)
  for k=1:length(pp_values)
 %  j=2;
 %  k=1;
    folder4pm_pp=strcat('pm_',num2str(pm_values(j),'%g'),'_pp_',num2str(pp_values(k),'%g'),'/')
    end_times=zeros(repetitions,1);
    for rep=1:repetitions

   %   rep=60;
      file_in=strcat(dir_up_global,folder4pm_pp,'percentage_wound_area_evolution_',num2str(rep,'%g'),'.mat');
      load(file_in);
      endtime=find(percentage_wound_area<1.5);

      if isempty(endtime)
      endtime=total_num_images+1;
      else
          endtime=endtime(1);
      end
        end_times(rep)=endtime;


    end

   file_out=strcat(dir_up_global,folder4pm_pp,'end_times.mat');
   save(file_out,'end_times');
  end
end
