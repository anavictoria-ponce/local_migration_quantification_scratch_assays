%this script read the files that netlogo produced (which are just the
%positions where the cells ) and changes the info to a domain with the
%cells set where the netlogo simulations stipulated
%file part of the source code of the manuscript "Local migration quantification method for scratch assays"
%----------------------------------------------------------------
%     Copyright (C) 2018 Ana Victoria Ponce Bobadilla
%     email: anavictoria.ponce@iwr.uni-heidelberg.de
%
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.
clc
clear all


in_silico_parameters

%------------------------------------------------
sides{1}='l';
sides{2}='r';
%-----------------------------------------------

%sim_evolutions2=cell(2,length(pm_values)*length(pp_values)*repetitions);

for j=1:length(pm_values)
    for k=1:length(pp_values)
  %         j=11;
      %       k=11;
 folder4pm_pp=strcat('pm_',num2str(pm_values(j),'%g'),'_pp_',num2str(pp_values(k),'%g'),'/')
       for rep=1:repetitions

   %         rep=1;


            evolution = cell(2,total_num_images+1);

            file_out=strcat(dir_up_global,folder4pm_pp,'evolution_pm_',num2str(pm_values(j),'%g'),'_pp_',num2str(pp_values(k),'%g'),'_',num2str(rep,'%g'),'.mat');
             %evolu2=zeros(total_num_images,sizeofdomain,sizeofdomain);
            for i=1:2 %this loop is to consider the evolution for the two sides
                for num_image=0:total_num_images
                    %    i=2;
                    %   num_image=0;

                    file_in=strcat(dir_up_global,folder4pm_pp,'occupancy_pm_',num2str(pm_values(j),'%g'),'_pp_',num2str(pp_values(k),'%g'),'_',num2str(rep,'%g'),'_numimg_',num2str(num_image,'%g'),'_',sides{i},'.dat');

                   evol = importdata(file_in);
                   evolu= imresize(evol,[sizeofdomain sizeofdomain],'nearest');%resizing the netlogo domain in the real domain
                   evolution{i,num_image+1}=evolu;
                   %  evolu2(num_image+1,:,:) = imresize(evol,[sizeofdomain sizeofdomain],'nearest');
                end
                % evolution{i}=evolu2;


            end% endo of the loop for the sides

             %sim_evolutions2(:,simulation_num)=evolution;

            save(file_out,'evolution');

        end %end of loop of repetitions

    end%end of looop of pp_values

end%end of loop of pm_values

%save('test2.mat','sim_evolutions2','-v7.3');
disp('Finished importing data');
