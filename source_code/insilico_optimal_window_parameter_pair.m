% code to provide the optimal window for each parameter combination
%file part of the source code of the manuscript "Local migration quantification method for scratch assays"
%----------------------------------------------------------------
%     Copyright (C) 2018 Ana Victoria Ponce Bobadilla
%     email: anavictoria.ponce@iwr.uni-heidelberg.de
%
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.
clc
clear all
close all

in_silico_parameters
for j=1:length(pm_values)
   for k=1:length(pp_values)

%        j=4;
%        k=1;
        folder4pm_pp=strcat('pm_',num2str(pm_values(j),'%g'),'_pp_',num2str(pp_values(k),'%g'),'/')
        fit_results_global=zeros(length(window_sizes),3);

        for rep=1:repetitions
   %         rep=1;

            file_fit=strcat(dir_up_global,folder4pm_pp,'fit_results_rep',num2str(rep,'%g'),'.mat');
            load(file_fit);
              fit_results_global(:,1)=fit_results_global(:,1)+sum(fit_squares_l_w,2)+sum(fit_squares_r_w,2);
        fit_results_global(:,2)=fit_results_global(:,2)+sum(fit_r2_l_w,2)+sum(fit_r2_r_w,2);
        fit_results_global(:,3)=fit_results_global(:,3)+fit_lr_distvel_ksdistance_w;

        end%end of repetitions
        fit_results_global_unscaled=fit_results_global;
num_scratches_analyzed=repetitions;
fit_results_global(:,1)=fit_results_global(:,1)/(2*sizeofdomain*num_scratches_analyzed);
fit_results_global(:,2)=fit_results_global(:,2)/(2*sizeofdomain*num_scratches_analyzed);
fit_results_global(:,3)=fit_results_global(:,3)/num_scratches_analyzed;



%to normalized the  residuals results
if (max(fit_results_global(:,1))-min(fit_results_global(:,1))>0)
    fit_resid_normalized=(max(fit_results_global(:,1))-fit_results_global(:,1))/(max(fit_results_global(:,1))-min(fit_results_global(:,1)));
else
    fit_resid_normalized=fit_results_global(:,1);
end

if (max(fit_results_global(:,2))-min(fit_results_global(:,2))>0)
    fit_r2_normalized=(fit_results_global(:,2)-min(fit_results_global(:,2)))/(max(fit_results_global(:,2))-min(fit_results_global(:,2)));
else
    fit_r2_normalized=fit_results_global(:,2);
end


%to normalize the lr velocity distribution distnace
if (max(fit_results_global(:,3))-min(fit_results_global(:,3))>0)
    fit_lr_normalized=(fit_results_global(:,3)-max(fit_results_global(:,3)))/(min(fit_results_global(:,3))-max(fit_results_global(:,3)));
else
    fit_lr_normalized=fit_results_global(:,3);
end

global_fit=1/3*(fit_resid_normalized+fit_lr_normalized+fit_r2_normalized);
max_index=find(max(global_fit)==global_fit);
optimal_window=max_index(1);
file_fit_results=strcat(dir_up_global,folder4pm_pp,'fit_results.mat');
save(file_fit_results,'optimal_window','global_fit','fit_results_global','fit_results_global_unscaled','optimal_window')


    end

end
