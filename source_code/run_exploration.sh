#!/bin/bash
#/*%tocompile the abm exploration
#%file part of the source code of the manuscript "Local migration quantification method for scratch assays"
#%----------------------------------------------------------------
#%     Copyright (C) 2018 Ana Victoria Ponce Bobadilla
#%     email: anavictoria.ponce@iwr.uni-heidelberg.de
#%
#%     This program is free software: you can redistribute it and/or modify
#%     it under the terms of the GNU General Public License as published by
#%     the Free Software Foundation, either version 3 of the License, or
#%     (at your option) any later version.
#%
#%     This program is distributed in the hope that it will be useful,
#%     but WITHOUT ANY WARRANTY; without even the implied warranty of
#%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#%     GNU General Public License for more details.
#%
#%     You should have received a copy of the GNU General Public License
#%     along with this program.  If not, see <https://www.gnu.org/licenses/>.*/

g++ -std=c++11 ABM_exploration.cc  -o abm_exploration;
./abm_exploration;
