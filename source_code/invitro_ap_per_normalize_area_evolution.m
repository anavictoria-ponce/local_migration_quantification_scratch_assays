%calculates the normalized wound area
%file part of the source code to perform the quantification method
%----------------------------------------------------------------
%     Copyright (C) 2018 Ana Victoria Ponce Bobadilla
%     email: anavictoria.ponce@iwr.uni-heidelberg.de
%
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.

%code to create the area info object that has the cell area and the holes area
%--------------------------------------------------
clc
clear all
close all

invitro_parameters

wound_area_info=strcat(dir4experimental_data,'wound_area_evolution.mat');
load(wound_area_info);

normalized_wound_area=zeros(num_scratches,total_num_images);

for s=1:num_scratches
    s
    initial_area=wound_area(s,1);
    for k=1:total_num_images
    normalized_wound_area(s,k)=100*wound_area(s,k)/initial_area;
    
    end
end

wound_name=strcat(dir4experimental_data,'percentage_wound_area_evolution.mat');
save(wound_name,'normalized_wound_area');


