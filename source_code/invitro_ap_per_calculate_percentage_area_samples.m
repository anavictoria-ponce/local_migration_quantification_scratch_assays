%calculates the percentage area samples
%file part of the statistical analysis to perform the quantification method
%----------------------------------------------------------------
%     Copyright (C) 2018 Ana Victoria Ponce Bobadilla
%     email: anavictoria.ponce@iwr.uni-heidelberg.de
%
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.
clc
clear all
close all

%code 2 defined the time 2 compare the areas againt the WT group

invitro_parameters;

time4comparison=zeros(1,5);%there are only 5 other groups


wound_name=strcat(dir4experimental_data,'percentage_wound_area_evolution.mat');
load(wound_name);

timename=strcat(dir4experimental_data,'time4comparison','.mat');
load(timename);
%%
wt_percentage_areas=zeros(num_samples,num_groups-1);

for group=1:num_groups-1
    rate_vec=[];
    
    for scratch=5:8
        
        rate_vec=[rate_vec; normalized_wound_area(scratch,time4comparison(group))];
        
    end
   wt_percentage_areas(:,group)= rate_vec;
    
end




group_percentage_areas=zeros(num_samples,num_groups-1);

for group=1:num_groups-1
    rate_vec=[];
    rate_vec2=[];
    
    for scratch=1+num_samples*(non_wt2group(group)-1):non_wt2group(group)*num_samples
        
        rate_vec=[rate_vec; normalized_wound_area(scratch,time4comparison(group))];

        %    rate_vec2=[    rate_vec2; scratch]
    end
group_percentage_areas(:,group)= rate_vec;
    
end




name=strcat(dir4experimental_data,'percentage_area_samples.mat');
save(name,'group_percentage_areas','wt_percentage_areas');


