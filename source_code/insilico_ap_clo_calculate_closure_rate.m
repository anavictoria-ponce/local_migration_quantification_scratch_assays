%code that calculates the closure rate
%file part of the source code of the manuscript "Local migration quantification method for scratch assays"
%----------------------------------------------------------------
%     Copyright (C) 2018 Ana Victoria Ponce Bobadilla
%     email: anavictoria.ponce@iwr.uni-heidelberg.de
%
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.
%--------------------------------------------------
clc
clear all
close all

in_silico_parameters
for j=1:length(pm_values)
    for k=1:length(pp_values)
        %j=5;
        %k=1;
        folder4pm_pp=strcat('pm_',num2str(pm_values(j),'%g'),'_pp_',num2str(pp_values(k),'%g'),'/')
        rate_closure=zeros(repetitions,1);
        for rep=1:repetitions
            %rep=1;
            file_in=strcat(dir_up_global,folder4pm_pp,'contour_evolution_pm_',num2str(pm_values(j),'%g'),'_pp_',num2str(pp_values(k),'%g'),'_',num2str(rep,'%g'),'.mat');
            file_in2=strcat(dir_up_global,folder4pm_pp,'area_evolution_',num2str(rep,'%g'),'.mat');
            %  save(file_out,'individual_wound_area');
            load(file_in);
            load(file_in2);

            total_num_images=size(contour_l_evolution,2);%total number of images

            x=0:total_num_images-1;
            x=0.5*x;

            p_l=polyfit(x, individual_wound_area(1:total_num_images),1);
            rate_closure(rep)=abs(p_l(1))/(2*sizeofdomain);

        end

        name=strcat(dir_up_global,folder4pm_pp,'closure_rate_samples.mat');
        save(name,'rate_closure');



    end
end
