%code to create the ideal initial condtion 
%file part of the source code of the manuscript "Local migration quantification method for scratch assays"
%----------------------------------------------------------------
%     Copyright (C) 2018 Ana Victoria Ponce Bobadilla
%     email: anavictoria.ponce@iwr.uni-heidelberg.de
%
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.

%-----------------------------------------------------------------------------------
clc
clear all
close all

%------------------------------------------------------------------------------------
in_silico_parameters

myformat = '%g %g \n';%format for the data to output %g

%------------------------------------------------------------------------------------


filename=strcat('experimental_data/ideal_initial_state4abm');

filename_l=strcat(filename,'_l.dat');
filename_r=strcat(filename,'_r.dat');

fileID_l = fopen(filename_l,'w');%the w is so it rewrites if the file has alreadz been created
fileID_r = fopen(filename_r,'w');

im_l4netlogo=zeros(sizeofartificialdomain,sizeofartificialdomain);
im_r4netlogo=zeros(sizeofartificialdomain,sizeofartificialdomain);
for i=1:sizeofartificialdomain
    for j=1:sizeofartificialdomain

        if(j<12)
            im_l4netlogo(i,j)=1;
        end

        if(j>38)
            im_r4netlogo(i,j)=1;
        end




        if im_l4netlogo(i,j)==1
            output_data=[i-1 j-1];
            fprintf(fileID_l,myformat,output_data);
        end

        if im_r4netlogo(i,j)==1
            output_data=[i-1 j-1];
            fprintf(fileID_r,myformat,output_data);
        end


    end
end

fclose(fileID_l);
fclose(fileID_r);
I=im_l4netlogo+im_r4netlogo;
imagesc(I)


namefile=strcat(dir4plots,'ideal_initial_condition4abm');
%printpng(fig1,namefile);
