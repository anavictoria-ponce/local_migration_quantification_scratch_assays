%performs the statistical analysis
%file part of the source code to perform the quantification method
%----------------------------------------------------------------
%     Copyright (C) 2018 Ana Victoria Ponce Bobadilla
%     email: anavictoria.ponce@iwr.uni-heidelberg.de
%
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.
%loading the velocities
clc
clear all
close all

invitro_parameters


filename2save_optimal_velocities=strcat(dir4experimental_data,'optimal_velocities.mat');
%save(filename2save_optimal_velocities,'individual_vel_index','windowed_optimal_vels','num_win_cells','num_win_cells4scratch5','group_vel_index','win_vel_groups','group_1_optimal_vels_cols');
load(filename2save_optimal_velocities);

normality_test=zeros(num_groups,1);
  data=group_1_optimal_vels_cols;
S=(data-mean(data))/sqrt(var(data));
normality_test(1)=kstest(S);

%-----------------------------------------------------------------------------------
%normality test
for i=1:num_groups-1
    data=win_vel_groups(:,i);
S=(data-mean(data))/sqrt(var(data));
normality_test(i+1)=kstest(S);


end


h_t=zeros(num_groups-1,1);
h_ks=zeros(num_groups-1,1);
h_r=zeros(num_groups-1,1);
p_t=zeros(num_groups-1,1);
p_ks=zeros(num_groups-1,1);
p_r=zeros(num_groups-1,1);


%-----------------------------------------------------------------------------------
%statistical tests


   for j=2:num_groups
        [h_t(j-1),p_t(j-1)]=ttest2(group_1_optimal_vels_cols,win_vel_groups(:,j-1));
        [h_ks(j-1),p_ks(j-1)]=kstest2(group_1_optimal_vels_cols,win_vel_groups(:,j-1));
        [p_r(j-1),h_r(j-1)]=ranksum(group_1_optimal_vels_cols,win_vel_groups(:,j-1));
   end

        hyp2plot=h_t;
         pval2plot=p_t;
%saving results in a table
       fprintf('\\begin{table}[H]\n')
        fprintf(' \\begin{center} \n')
        fprintf(' \\scalebox{0.8}{ \n')
 fprintf(' \\begin{tabular}{|  c  |  C{2.25cm}  |  C{2.25cm} |  C{2.25cm} | C{2.25cm} |  C{2.25cm} | C{2.25cm} |  }	\\hline  \n')
 fprintf(' & S2 & S3 & S4 & S5& S6 \\\\  \\hline \n ')
 fprintf('S1 & ')
 for i=1:4
     fprintf('h=%d, p=%1.3e & ',hyp2plot(i),pval2plot(i))
 end
 fprintf('h=%d, p=%1.3e \\\\  \\hline \n ',hyp2plot(5),pval2plot(5))
 fprintf(' \\end{tabular} } \n')
 fprintf(' \\end{center}\\caption{Results of t-tests between the different groups velocities \\label{tab:ttest}} \n')
 fprintf(' \\end{table} \n')
 
 
 
 hyp2plot=h_t;
         pval2plot=p_t;
%saving results in a table
       fprintf('\\begin{table}[H]\n')
        fprintf(' \\begin{center} \n')
        fprintf(' \\scalebox{0.8}{ \n')
 fprintf(' \\begin{tabular}{|   C{2.25cm} |  C{2.25cm}  |  C{2.25cm} |  C{2.25cm} | C{2.25cm} |  C{2.25cm} | C{2.25cm}|  }	\\hline  \n')
 fprintf(' & S2 & S3 & S4 & S5& S6 \\\\  \\hline \n ')
 fprintf('t-test & ')
 for i=1:4
     fprintf('h=%d, p=%1.3e & ',hyp2plot(i),pval2plot(i))
 end
 fprintf('h=%d, p=%1.3e \\\\  \\hline \n ',hyp2plot(5),pval2plot(5))

         hyp2plot=h_r;
           pval2plot=p_r;
 fprintf('  Wilcoxon rank sum test & ')
 for i=1:4
     fprintf('h=%d, p=%1.3e & ',hyp2plot(i),pval2plot(i))
 end
 fprintf('h=%d, p=%1.3e \\\\  \\hline \n ',hyp2plot(5),pval2plot(5))
 
 
    hyp2plot=h_ks;
         pval2plot=p_ks;
 

 fprintf('ks-test & ')
 for i=1:4
     fprintf('h=%d, p=%1.3e & ',hyp2plot(i),pval2plot(i))
 end
 fprintf('h=%d, p=%1.3e \\\\  \\hline \n ',hyp2plot(5),pval2plot(5))
 fprintf(' \\end{tabular} } \n')
 fprintf(' \\end{center}\\caption{Windowed velocity \\label{tab:ks_test}} \n')
 fprintf(' \\end{table} \n')
 
 
