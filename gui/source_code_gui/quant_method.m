%GUI main file to perform the quantification method
%----------------------------------------------------------------
%     Copyright (C) 2018 Ana Victoria Ponce Bobadilla
%     email: anavictoria.ponce@iwr.uni-heidelberg.de
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.
function varargout = quant_method(varargin)

% QUANT_METHOD MATLAB code for quant_method.fig
%      QUANT_METHOD, by itself, creates a new QUANT_METHOD or raises the existing
%      singleton*.
%
%      H = QUANT_METHOD returns the handle to a new QUANT_METHOD or the handle to
%      the existing singleton*.
%
%      QUANT_METHOD('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in QUANT_METHOD.M with the given input arguments.
%
%      QUANT_METHOD('Property','Value',...) creates a new QUANT_METHOD or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before quant_method_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to quant_method_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help quant_method


% Last Modified by GUIDE v2.5 16-Jun-2018 19:22:10


% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @quant_method_OpeningFcn, ...
    'gui_OutputFcn',  @quant_method_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end

% End initialization code - DO NOT EDITend

% --- Executes just before quant_method is made visible.
function quant_method_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to quant_method (see VARARGIN)

% Choose default command line output for quant_method
handles.output = hObject;

% Update handles structure
handles.StopNow = 0;
handles.dir4_each_experiment='/exp_';
handles.sizeofdomain=500;
handles.window_sizes=1:1:handles.sizeofdomain;
handles.filename4_unsegmentedscratch = 'unsegmented_scratch_';
handles.std_deviation_threshold=500;
handles.segmenting_text='';
handles.output_folder='';
handles.sampling_factor=1;
t = handles.table_normality_test;
handles.output_folder_structure_created=0;

d = zeros(1,3); % Make some random data to add

set(t,'Data',d); % Use the set command to change the uitable properties.

set(t,'ColumnName',{'Group:';'p-value:';'Test decision:'});
handles.info_entered=0;
guidata(hObject, handles);


% UIWAIT makes quant_method wait for user response (see UIRESUME)
%uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = quant_method_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in load_button.
function load_button_Callback(hObject, eventdata, handles)

dir4experimental_data = uigetdir;
handles.exp_data_folder_text.String=dir4experimental_data;
% hObject    handle to load_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
guidata(hObject, handles);


% --- Executes on button press in enter_data.
function enter_data_Callback(hObject, eventdata, handles)

% hObject    handle to enter_data (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

prompt = {'Number of experiments:','Number of scratches per experiment:','Number of cell groups:','Number of measurements per group in an experiment:'};
title = 'Experiment information';
dims = [1 60];
definput = {'4','3','3','1'};
answer = inputdlg(prompt,title,dims,definput);
check_empty=isempty(answer);
if (check_empty~=1)
handles.num_experiments=str2double(answer{1});
handles.num_scratches=str2double(answer{2});
handles.num_groups=str2double(answer{3});
handles.num_measurements_per_exp=str2double(answer{4});
groups_string= cell(1,handles.num_groups);
for i=1:handles.num_groups
    groups_string{i}=num2str(i);
end

handles.listbox_group2test.String=groups_string;
handles.listbox_group2test_against.String=groups_string;
handles.listbox_groupsallagainst.String=groups_string;
handles.info_entered=1;

handles.status_text.String='Experimental information entered.';
     drawnow;
     pause(1);
     handles.status_text.String=' ';
end
guidata(hObject, handles);


% --- Executes on button press in ouput_folder_pushbutton.
function ouput_folder_pushbutton_Callback(hObject, eventdata, handles)
prompt = {'Folder name'};
title = 'Enter the name of the folder which will contain the quantification results';
dims = [1 110];
definput = {'quantification_results'};

answer = inputdlg(prompt,title,dims,definput);
check_empty=isempty(answer);
if (check_empty~=1)
dirname=answer{1};

handles.output_folder_prefix = uigetdir('pwd','Select the location of the folder');

handles.output_folder_text.String=strcat(handles.output_folder_prefix ,'/',dirname);
handles.output_folder=handles.output_folder_text.String;
mkdir(handles.output_folder_text.String);
handles.output_folder_structure_created=1;
end
guidata(hObject, handles);
% hObject    handle to ouput_folder_pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in run_segmentation.
function run_segmentation_Callback(hObject, eventdata, handles)
segmentation_algorithm
handles.status_text.String ='Done!';
drawnow;


guidata(hObject, handles);
% hObject    handle to run_segmentation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1


% --- Executes on button press in decompress_files_button.
function decompress_files_button_Callback(hObject, eventdata, handles)
if ((handles.info_entered==1)&&( handles.output_folder_structure_created==1))

    un_tiff_files

else

    f = errordlg('No information about the experiment has been specified or folder structure has not been prepared ');
end
guidata(hObject, handles);


% hObject    handle to decompress_files_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in show_segmentation_button.
function show_segmentation_button_Callback(hObject, eventdata, handles)
[file,path] = uigetfile('.jpg','Select file to view segmentation');
file2segment=[path, file];
if (exist(file2segment, 'file')==2)
    axes(handles.segmentation_plot);
    cla;
    I=imread([path, file]);
    imagesc(I);
    set(gca,'YDir','normal');
    set(gca,'xtick',[],'ytick',[]);
    handles.status_text.String =strcat('Viewing file: ',[path, file]);
else

    string=['The file ',file2segment,' does not exist. You need to detect cell fronts.'];
    f = errordlg(string);
end
% hObject    handle to show_segmentation_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in stop_button.
function stop_button_Callback(hObject, eventdata, handles)
handles.StopNow = 1;
 guidata(hObject, handles);
% hObject    handle to stop_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in calc_velocities.
function calc_velocities_Callback(hObject, eventdata, handles)

calculate_window_velocities

 guidata(hObject, handles);

% --- Executes on button press in plot_vels.
function plot_vels_Callback(hObject, eventdata, handles)
axes(handles.ind_velocities);
cla;
filename=strcat(handles.output_folder,'/optimal_windowed_velocities.mat');
if (exist(filename, 'file')==2)
    load(filename);

    boxplot(velocities4all)
    set(gca,'FontName','LM Roman 10','FontSize', 12)
    output_name='boxplots';
    set(gca, 'FontSize', 12)
    xlhand = get(gca,'xlabel');
    set(xlhand,'string','Scratches','fontsize',12)
    ylhand = get(gca,'ylabel');
    set(ylhand,'string','Velocity (\mu m/h) ','fontsize',12)
else
    string=['The velocities need to be calculated before.'];
    f = errordlg(string);
end
% hObject    handle to plot_vels (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in save_plot.
function save_plot_Callback(hObject, eventdata, handles)
[file,path] = uiputfile('individual_velocities','Save plot of velocities as');
filename=[path,file];

fignew=figure('Visible','off','pos',[10 10 470 310]);
%  fignew = figure('Visible','off'); % Invisible figure
copyobj(handles.ind_velocities, fignew);

saveas(gca, filename,'png');
delete(fignew);



% --- Executes on button press in plot_group_velocities.
function plot_group_velocities_Callback(hObject, eventdata, handles)
axes(handles.group_vels_plot);
cla;
filename=strcat(handles.output_folder,'/optimal_windowed_velocities.mat');
 if (exist(filename, 'file')==2)
load(filename);
boxplot(velocity_groups_measurement)
set(gca,'FontName','LM Roman 10','FontSize', 12)
set(gca, 'FontSize', 12)
xlhand = get(gca,'xlabel');
set(xlhand,'string','Cell groups','fontsize',12)
ylhand = get(gca,'ylabel');
set(ylhand,'string','Velocity (\mu m/h) ','fontsize',12)
else
    string=['The velocities need to be calculated before.'];
    f = errordlg(string);
end



% hObject    handle to plot_group_velocities (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2


% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in save_plot_group_velocities.
function save_plot_group_velocities_Callback(hObject, eventdata, handles)
[file,path] = uiputfile('group_velocities','Save plot of group velocities as');
filename=[path,file];
fignew=figure('Visible','off','pos',[10 10 470 310]);
filename_vels=strcat(handles.output_folder,'/optimal_windowed_velocities.mat');
load(filename_vels);
boxplot(velocity_groups_measurement)
xlabel('Cell groups','fontsize',14,'FontName','LM Roman 10')
ylabel('Velocity (\mu m/h) ','fontsize',14,'FontName','LM Roman 10')

% ylim([0 10]);
saveas(fignew, filename,'png');
%delete(fignew);


% --- Executes on button press in save_velocities.
function save_velocities_Callback(hObject, eventdata, handles)
[file,path] = uiputfile('individual_velocities.txt');

if (file~=0)
    
    filename=strcat(handles.output_folder,'/optimal_windowed_velocities.mat');
    if (exist(filename, 'file')==2)
        load(filename);
        ind_vels_file=[path,file];
        dlmwrite(ind_vels_file, velocities4all,'delimiter','\t','precision',3);
    else
        string=['The velocities need to be calculated before.'];
        f = errordlg(string);
    end
else
    string=['No filename has been entered. The velocities have not been saved. '];
    f = errordlg(string);
end

% hObject    handle to save_velocities (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in save_group_velocities.
function save_group_velocities_Callback(hObject, eventdata, handles)
[file,path] = uiputfile('group_velocities.txt');
if (file~=0)
    filename=strcat(handles.output_folder,'/optimal_windowed_velocities.mat');
    if (exist(filename, 'file')==2)
        
        load(filename);
        group_vels_file=[path,file];
        dlmwrite(group_vels_file,velocity_groups_measurement,'delimiter','\t','precision',3);
    else
        string=['The velocities need to be calculated before.'];
        f = errordlg(string);
    end
else
    string=['No filename has been entered. The velocities have not been saved. '];
    f = errordlg(string);
end


% --- Executes on selection change in popupmenu3.
function popupmenu3_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu3


% --- Executes during object creation, after setting all properties.
function popupmenu3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu_normality_test.
function popupmenu_normality_test_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_normality_test (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_normality_test contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_normality_test


% --- Executes during object creation, after setting all properties.
function popupmenu_normality_test_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_normality_test (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in listbox_stat_test_comparison.
function listbox_stat_test_comparison_Callback(hObject, eventdata, handles)
% hObject    handle to listbox_stat_test_comparison (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox_stat_test_comparison contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox_stat_test_comparison


% --- Executes during object creation, after setting all properties.
function listbox_stat_test_comparison_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox_stat_test_comparison (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in radiobutton1.
function radiobutton1_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton1


% --- Executes on button press in radiobutton2.
function radiobutton2_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton2


% --- Executes on button press in perform_test_normality.
function perform_test_normality_Callback(hObject, eventdata, handles)

normality_test_groups
guidata(hObject, handles);


% --- Executes on button press in statistical_comparison_button
function statistical_comparison_button_Callback(hObject, eventdata, handles)

statistical_comparison_test
guidata(hObject, handles);


% --- Executes on selection change in listbox_group2test.
function listbox_group2test_Callback(hObject, eventdata, handles)
% hObject    handle to listbox_group2test (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox_group2test contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox_group2test


% --- Executes during object creation, after setting all properties.
function listbox_group2test_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox_group2test (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in listbox_group2test_against.
function listbox_group2test_against_Callback(hObject, eventdata, handles)
% hObject    handle to listbox_group2test_against (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox_group2test_against contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox_group2test_against


% --- Executes during object creation, after setting all properties.
function listbox_group2test_against_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox_group2test_against (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in radiobutton3.
function radiobutton3_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton3


% --- Executes on selection change in listbox_groupsallagainst.
function listbox_groupsallagainst_Callback(hObject, eventdata, handles)
% hObject    handle to listbox_groupsallagainst (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox_groupsallagainst contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox_groupsallagainst


% --- Executes during object creation, after setting all properties.
function listbox_groupsallagainst_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox_groupsallagainst (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in save_results_normality_test_button.
function save_results_normality_test_button_Callback(hObject, eventdata, handles)
[file,path] = uiputfile('normality_test_results.txt');
if (file~=0)
filename=[path,file];
fileID = fopen(filename,'w');
fprintf(fileID,'Normality test using the Kolmogorov-smirnov test to the group velocities \n');
fprintf(fileID,'Group:              p-vale:             Test decision:\n');
for i=1:handles.num_groups

    fprintf(fileID,'%g     %6.4f     %g\n',handles.normality_tests_results(i,1),handles.normality_tests_results(i,2),handles.normality_tests_results(i,3));
end
fclose(fileID);
else
    string=['No filename has been entered. The results of the normality test have not been saved. '];
    f = errordlg(string);
end

% hObject    handle to save_results_normality_test_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in save_results_comparison_test.
function save_results_comparison_test_Callback(hObject, eventdata, handles)
plot_results_comparison_test

% --- Executes during object creation, after setting all properties.
function table_normality_test_CreateFcn(hObject, eventdata, handles)
% hObject    handle to table_normality_test (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes when entered data in editable cell(s) in table_normality_test.
function table_normality_test_CellEditCallback(hObject, eventdata, handles)
% hObject    handle to table_normality_test (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.TABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in detect_fronts.
function detect_fronts_Callback(hObject, eventdata, handles)

segmentation_algorithm
if (flag1==0)
    drawnow;
    detecting_contour
    handles.status_text.String ='Done!';
    drawnow;
    pause(1);
    handles.status_text.String =' ';
    drawnow;
end
guidata(hObject, handles);


% --- Executes on button press in change_axis_limits.
function change_axis_limits_Callback(hObject, eventdata, handles)
prompt = {'Min y value','Max y value'};
title = 'Enter y-axis limits:';
dims = [1 35];
definput = {'0','10'};
answer = inputdlg(prompt,title,dims,definput);
ymin=str2double(answer{1});
ymax=str2double(answer{2});
axes(handles.group_vels_plot);
ylim([ymin ymax]);


% hObject    handle to change_axis_limits (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in change_axis_limit_ind_vels_plot.
function change_axis_limit_ind_vels_plot_Callback(hObject, eventdata, handles)
prompt = {'Min y value','Max y value'};
title = 'Enter y-axis limits:';
dims = [1 35];
definput = {'0','10'};
answer = inputdlg(prompt,title,dims,definput);
ymin=str2double(answer{1});
ymax=str2double(answer{2});
axes(handles.ind_velocities);
ylim([ymin ymax]);

% hObject    handle to change_axis_limit_ind_vels_plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in prepare_output_folder.
function prepare_output_folder_Callback(hObject, eventdata, handles)

if (handles.info_entered==1)
    if ((handles.output_folder_structure_created==1)&&(isempty(handles.exp_data_folder_text)==0))

        
        for j=1:handles.num_experiments
            name4exp_folder=strcat(handles.output_folder_text.String,'/','exp_',num2str(j,'%g'));
            mkdir(name4exp_folder);
            for i = 1:handles.num_scratches
                name4scratchfolder=strcat(handles.output_folder_text.String,'/','exp_',num2str(j,'%g'),'/',num2str(i,'%g'));
                mkdir(name4scratchfolder);
                
            end
            
        end

        handles.status_text.String ='Done!';
handles.output_folder_structure_created=1;
        drawnow;
        pause(2);
        handles.status_text.String =' ';

        drawnow;

        handles.output_folder_structure_created=1;
    else
        f = errordlg('The otput or the experimental folder has not been determined');
    end
else

    f = errordlg('No information about the experiment has been specified');
end
guidata(hObject, handles);


% --- Executes on button press in about_button.
function about_button_Callback(hObject, eventdata, handles)

f=msgbox({'GUI to perform local quantification method';'Copyright (C) 2018 Ana Victoria Ponce Bobadilla';'email: anavictoria.ponce@iwr.uni-heidelberg.de';'License: GNU/GPLv3' } );

%GUI main file to perform the quantification method
%----------------------------------------------------------------
%     Copyright (C) 2018 Ana Victoria Ponce Bobadilla
%     email: anavictoria.ponce@iwr.uni-heidelberg.de
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.
% hObject    handle to about_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
