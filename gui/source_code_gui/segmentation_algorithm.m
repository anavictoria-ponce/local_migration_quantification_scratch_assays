%perfroms the segmentation algorithm
%file part of the GUI to perform the quantification method
%----------------------------------------------------------------
%     Copyright (C) 2018 Ana Victoria Ponce Bobadilla
%     email: anavictoria.ponce@iwr.uni-heidelberg.de
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.

%addpath files4growcut_segmentation_algorithm

flag1=0;
flag2=0;


for exp=1:handles.num_experiments
    %    exp=1;
    
    
    filename2save_num=strcat(handles.output_folder,handles.dir4_each_experiment,num2str(exp,'%g'),'/number_pictures_4scratch.mat');
    
    if (exist(filename2save_num, 'file')==2)
        load(filename2save_num);%loads the number of pictures 4 scratch % the number of images that were actually on the tiff %number_pictures_4scratch
        
        %this file will save the numbers of scratches for which it was possible
        %to make the segmentation
        filename2save_num_aftersegmentation=strcat(handles.output_folder,handles.dir4_each_experiment,num2str(exp,'%g'),'/number_pictures_4scratch_aftersegmentation.mat');
        number_pictures_4scratch_aftersegmentation=zeros(handles.num_scratches,1);
        
        for s=1:handles.num_scratches
            
            
            %s=1;
            num_images=number_pictures_4scratch(s);
            folder_out=strcat(handles.output_folder,handles.dir4_each_experiment,num2str(exp,'%g'),'/',...,
                num2str(s,'%g'),'/');
            for k = 1:num_images
                
                handles.status_text.String=['Performing the segmentation of scratch ', num2str(s,'%g'),' in experiment ',num2str(exp,'%g'),...,
                    '      Progress: ', num2str(handles.num_scratches*(exp-1)+s-1,'%g'),'/ ',num2str(handles.num_scratches*handles.num_experiments,'%g'),...,
                    ' of total scratches and ',num2str(k,'%g'),'/ ',num2str(num_images,'%g'),' of the images within this scratch'];
                
                drawnow;
                
                %saving the number of scratches after segmentation
                number_pictures_4scratch_aftersegmentation(s)=k;
                %k
                %k =1;
                file4_unsegmented_img=strcat(folder_out, handles.filename4_unsegmentedscratch,num2str(k,'%g'),'.png');
                
                if (exist(file4_unsegmented_img, 'file')==2)
                    I=imread( file4_unsegmented_img);
                    
                    %resizing the image since 1 pixel =2 mu m
                    I=imresize(I,0.5);
                    
                    %defining the labels for the growcut algorithm
                    BW1 = edge(I,'canny');
                    BW2 = edge(I,'roberts');
                    BW3=BW2 | BW1;
                    labels=BW3;
                    labels=double(labels);
                    
                    %calculating the std deviation of the image density
                    std_img=movingstd2(I,2);
                    
                    %seting the region where the image intensity does not change much
                    BW4=std_img<handles.std_deviation_threshold;
                    BW4=imopen((BW4),strel('disk',round(18/handles.sampling_factor),0));
                    BW4=imclose((BW4),strel('disk',round(18/handles.sampling_factor),0));
                    labels(BW4)=-1;
                    
                    if (sum(sum(BW4))==0)
                        %  msgbox('there is no pixels that satisfy std_img<500, no background pixels, growcut wont work');%this message does not work when matlab is run from the terminal
                        break; % we need to include the break when the code does not detect any pixels that fulfill <std_deviation_threshold, usually happens when the wound is closed
                    end
                    [detected_contour, strengths] = growcut(uint8(std_img),double(labels));
                    
                    %saving the segmentated_img4detecting the contour later
                    filename_4detectedcontour=strcat(folder_out,'detected_contour_',num2str(k,'%g'),'.mat');
                    save(filename_4detectedcontour,'detected_contour');
                    
                    %plotting the segmented_img
                    f = figure('visible','off');
                    contour_out_pos = edge(labels,'canny');
                    cont2=imcontour(detected_contour);
                    imagesc(I)
                    set(gca,'YDir','normal')
                    hold on
                    plot(cont2(1,:),cont2(2,:),'b.','markersize',5)
                    colormap gray
                    hold off
                    set(gca,'xtick',[],'ytick',[])
                    filename_4img_detectedcontour=strcat(folder_out,'segmented_scratch_',num2str(k,'%g'),'.jpg');
                    saveas(f, filename_4img_detectedcontour);
                else
                    string=['The file ',filename2save_num,' does not exist. You need to decompress the images before.'];
                    f = errordlg(string);
                    flag1=1;
                    break;
                end
                
                
                
            end
            
            if(flag1==1)
                flag2=1;
                break;
            end
            
        end
        
        
        save(filename2save_num_aftersegmentation,'number_pictures_4scratch_aftersegmentation')
        if(flag2==1)
            break;
        end
    else
        string=['The file ',filename2save_num,' does not exist. You need to decompress the images before.'];
        f = errordlg(string);
        flag1=1;
        break;
    end
end

