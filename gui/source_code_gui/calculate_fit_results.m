%calculates the optimal_window
%file part of the GUI to perform the quantification method
%----------------------------------------------------------------
%     Copyright (C) 2018 Ana Victoria Ponce Bobadilla
%     email: anavictoria.ponce@iwr.uni-heidelberg.de
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.

fit_results_global=zeros(length(handles.window_sizes),3);

for exp=1:handles.num_experiments
    % exp=1;
  %  exp

    for s=1:handles.num_scratches




        fit_r2_l_w=zeros(length(handles.window_sizes),handles.sizeofdomain);
        fit_r2_r_w=zeros(length(handles.window_sizes),handles.sizeofdomain);
        fit_squares_l_w=zeros(length(handles.window_sizes),handles.sizeofdomain);
        fit_squares_r_w=zeros(length(handles.window_sizes),handles.sizeofdomain);
        fit_lr_distvel_ksdistance_w=zeros(length(handles.window_sizes),1);


        folder4_exp_s=strcat(handles.output_folder,handles.dir4_each_experiment,num2str(exp,'%g'),'/',...,
            num2str(s,'%g'),'/');
        file_windowed_velocities=strcat(folder4_exp_s,'windowed_velocities.mat');
        load(file_windowed_velocities);
        file4contour_evolution=strcat( folder4_exp_s,'contour_evolution.mat');
        load(file4contour_evolution);

        total_num_images=size(contour_l_evolution,2);%total number of images
        x=0:total_num_images-1;
        x=0.5*x;

        for window_size=1:length(handles.window_sizes)

          handles.status_text.String=['Calculating optimal window with respect to scratch ', num2str(s,'%g'),' in experiment ',num2str(exp,'%g'),...,
              '        Progress:  ', num2str(handles.num_scratches*(exp-1)+s-1,'%g'),'/',num2str(handles.num_scratches*handles.num_experiments,'%g')];
          drawnow;



            %        window_size=1;
            %making the domain partition based on the window size
            windowed_subdomain=window_size:window_size:handles.sizeofdomain;
            windowed_subdomain=[ 0 windowed_subdomain];
            if(windowed_subdomain(end)~=handles.sizeofdomain)
                windowed_subdomain=[windowed_subdomain handles.sizeofdomain ];
            end
            num_win_cells= length( windowed_subdomain)-1;


            %loop over all the y coordinates
            for i=1:num_win_cells% for the number of subdomains
                %    i=1;
                init=windowed_subdomain(i)+1;
                endd=windowed_subdomain(i+1);

                yfit_l=vel_l_w(window_size,i)*x+xintercept_l_w(window_size,i);
                yfit_r=vel_r_w(window_size,i)*x+xintercept_r_w(window_size,i);

                for line_num=init:endd %loops over the pixels of the pixels within each window
                    %    line_num=init;
                    % fit for the left contour
                    interfacel_evolution=contour_l_evolution(line_num,1:end);
                    yresid = interfacel_evolution - yfit_l;

                    % fit for the left contour: sum of residual squares
                    SSresid = sum(yresid.^2);
                    fit_squares_l_w(window_size,line_num) =SSresid/handles.sizeofdomain;
                    SStotal = (length(interfacel_evolution)-1) * var(interfacel_evolution);

                    % fit for the left contour: R2 coefficient of
                    % determination
                    if SStotal~=0
                        fit_r2_l_w(window_size,line_num) = 1 - SSresid/SStotal;
                    else%this is the case where var(interfacel_evolution)=0
                        if (abs(xintercept_l_win(i,rep)-2)<1e-10)
                            fit_r2_l_w(window_size,line_num)=0;
                        else
                            fit_r2_l_w(window_size,line_num) =1;
                        end

                    end

                    % fit for right contour
                    interface_r_evolution=handles.sizeofdomain-contour_r_evolution(line_num,1:end);
                    yresid = interface_r_evolution - yfit_r;

                    % fit for right contour: sum of residual squares
                    SSresid = sum(yresid.^2);
                    fit_squares_r_w(window_size,line_num) =SSresid/handles.sizeofdomain; %scaled
                    SStotal = (length(interface_r_evolution)-1) * var(interface_r_evolution);

                    % fit for right contour: r2 coefficient of
                    % determination
                    if SStotal~=0
                        fit_r2_r_w(window_size,line_num) = 1 - SSresid/SStotal;
                    else
                        if (abs(xintercept_l_win(i,rep)-2)<1e-10)
                            fit_r2_r_w(window_size,line_num)=0;
                        else
                            fit_r2_r_w(window_size,line_num) =1;
                        end
                    end

                end%end of the loop over the index in each subdomain

            end%end loop over num win cells

            %to calcualte the distance of the velocity distributions
            n     = num_win_cells^2  /(2*num_win_cells);
            n=sqrt(n);%to have the coefficient that takes into account the number of the sample for the distance in the ks test
            [h,p,ks2stat] = kstest2(vel_l_w(window_size,1:num_win_cells),vel_r_w(window_size,1:num_win_cells));
            fit_lr_distvel_ksdistance_w(window_size)=ks2stat*n;

        end  %end if window sizes

        %code to save the values of the fit function


        fit_results_global(:,1)=fit_results_global(:,1)+sum(fit_squares_l_w,2)+sum(fit_squares_r_w,2);
        fit_results_global(:,2)=fit_results_global(:,2)+sum(fit_r2_l_w,2)+sum(fit_r2_r_w,2);
        fit_results_global(:,3)=fit_results_global(:,3)+fit_lr_distvel_ksdistance_w;


    end%end of the loop over the scratches
end%end of the loop over the experiments

%%

   handles.status_text.String=['Calculating optimal window with respect to scratch  ', num2str(s,'%g'),' in experiment  ',num2str(exp,'%g'),...,
            '  Progress:  ', num2str(handles.num_scratches*(exp-1)+s,'%g'),'/',num2str(handles.num_scratches*handles.num_experiments,'%g')];
        drawnow;

        pause(1);

   handles.status_text.String=strcat('Calculating optimal window ');
        drawnow;
        pause(1);
%fit_results_global=fit_results_global_unscaled;
 fit_results_global_unscaled=fit_results_global;
num_scratches_analyzed=handles.num_experiments*handles.num_scratches;
fit_results_global(:,1)=fit_results_global(:,1)/(2*handles.sizeofdomain*num_scratches_analyzed);
fit_results_global(:,2)=fit_results_global(:,2)/(2*handles.sizeofdomain*num_scratches_analyzed);
fit_results_global(:,3)=fit_results_global(:,3)/num_scratches_analyzed;



%to normalized the  residuals results
if (max(fit_results_global(:,1))-min(fit_results_global(:,1))>0)
    fit_resid_normalized=(max(fit_results_global(:,1))-fit_results_global(:,1))/(max(fit_results_global(:,1))-min(fit_results_global(:,1)));
else
    fit_resid_normalized=fit_results_global(:,1);
end

if (max(fit_results_global(:,2))-min(fit_results_global(:,2))>0)
    fit_r2_normalized=(fit_results_global(:,2)-min(fit_results_global(:,2)))/(max(fit_results_global(:,2))-min(fit_results_global(:,2)));
else
    fit_r2_normalized=fit_results_global(:,2);
end


%to normalize the lr velocity distribution distnace
if (max(fit_results_global(:,3))-min(fit_results_global(:,3))>0)
    fit_lr_normalized=(fit_results_global(:,3)-max(fit_results_global(:,3)))/(min(fit_results_global(:,3))-max(fit_results_global(:,3)));
else
    fit_lr_normalized=fit_results_global(:,3);
end


global_fit=1/3*(fit_resid_normalized+fit_lr_normalized+fit_r2_normalized);
max_index=find(max(global_fit)==global_fit);
optimal_window=max_index(1);
handles.optimal_window=optimal_window;

    filename=strcat(handles.output_folder,'/optimal_window.mat');
    save(filename,'optimal_window');
       handles.status_text.String=' ';
