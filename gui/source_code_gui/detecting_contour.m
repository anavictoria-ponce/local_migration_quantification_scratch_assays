%detects the contour 
%file part of the GUI to perform the quantification method
%----------------------------------------------------------------
%     Copyright (C) 2018 Ana Victoria Ponce Bobadilla
%     email: anavictoria.ponce@iwr.uni-heidelberg.de
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.

for exp=1:handles.num_experiments

    
    %load the number of files for which the segmentation were performed
    filename2save_num_aftersegmentation=strcat(handles.output_folder,handles.dir4_each_experiment,num2str(exp,'%g'),'/number_pictures_4scratch_aftersegmentation.mat');
    load(filename2save_num_aftersegmentation);%loads the number of pictures 4 scratch % the number of images that were actually on the tiff %number_pictures_4scratch
    
    for s=1:handles.num_scratches

       
        num_images=number_pictures_4scratch_aftersegmentation(s);
        
        folder4_exp_s=strcat(handles.output_folder,handles.dir4_each_experiment,num2str(exp,'%g'),'/',...,
            num2str(s,'%g'),'/');
           
        %file where we will save the contour_evolution
        file4contour_evolution=strcat(folder4_exp_s,'contour_evolution.mat');
        
        contour_l_evolution=[];
        contour_r_evolution=[];
        
        for k = 1:num_images
            
                            
                handles.status_text.String=['Detecting the leading edges of scratch ', num2str(s,'%g'),' in experiment ',num2str(exp,'%g'),...,
                    '      Progress: ', num2str(handles.num_scratches*(exp-1)+s-1,'%g'),'/ ',num2str(handles.num_scratches*handles.num_experiments,'%g'),...,
                    'of total scratches and ',num2str(k,'%g'),'/ ',num2str(num_images,'%g'),' of the images within this scratch'];
                
                drawnow;
      
            filename_4detectedcontour=strcat(folder4_exp_s,'detected_contour_',num2str(k,'%g'),'.mat');
            load(filename_4detectedcontour);
            
            labels=bwlabel(detected_contour);% we need bwlabel for using regionprops
            reg=regionprops(labels);
            
            %imagesc(labels) % to plot the labels
            
            %creates a vector of the areas so we can identify the max
            areas = [reg.Area];
            % This is to identify the two largest areas (which are the cell masses)
            index1=find(areas==max(areas));
            
            if  length(index1)>1
                tmp=index1;
                index1=tmp(1);
                index2=tmp(2);
            else
                
                index2=find(areas==max(areas(areas~=max(areas))));
            end
            
            if  (reg(index1).BoundingBox(1)<2) && (reg(index1).BoundingBox(3)>((handles.sizeofdomain)-2))
                break; %this if is so the code breaks when the two fronts have touched
            end
            
            % we consider the bounding box so we can identify the left and the right
            % masses
            if (reg(index1).BoundingBox(1)<100) && (reg(index1).BoundingBox(2)<100)
                index_l=index1;
                index_r=index2;
            else
                index_l=index2;
                index_r=index1;
            end % index_l is for the left mass and simlarwise for the right
            
            %we defined the two cell front according to the index of the largest areas
        im_l=ismember(labels, index_l);
        im_r=ismember(labels, index_r);

                max_u=max(find(im_l(3,:)));
        im_l(1,1:max_u)=1;
        im_l(2,1:max_u)=1;
        max_u=max(find(im_l(end-3,:)));
        im_l(end,1:max_u)=1;
        im_l(end-1,1:max_u)=1;
        im_l(:,1)=1;

        min_u=min(find(im_r(3,:)));
        im_r(1,min_u:end)=1;
        im_r(2,min_u:end)=1;

        min_u=min(find(im_r(end-3,:)));
        im_r(end,min_u:end)=1;
        im_r(end-1,min_u:end)=1;
        im_r(:,end)=1;

        im_l = bwmorph(im_l,'diag');
    im_l = bwmorph(im_l,'majority');
    im_l = bwmorph(im_l,'close');

    im_r = bwmorph(im_r,'diag');
    im_r = bwmorph(im_r,'majority');
    im_r = bwmorph(im_r,'close');


                    % filling the holes
                    im_l=imfill(im_l,'holes');
                    im_r=imfill( im_r,'holes');


                    im_l = bwareaopen(im_l, 50);
                    im_r = bwareaopen(im_r, 50);

        max_u=max(find(im_l(3,:)));
        im_l(1,1:max_u)=1;
        im_l(2,1:max_u)=1;

        max_u=max(find(im_l(end-3,:)));
        im_l(end,1:max_u)=1;
        im_l(end-1,1:max_u)=1;

        min_u=min(find(im_r(3,:)));
        im_r(1,min_u:end)=1;
        im_r(2,min_u:end)=1;

        min_u=min(find(im_r(end-3,:)));
        im_r(end,min_u:end)=1;
        im_r(end-1,min_u:end)=1;

        %we added this to detect later when calculating velocities which is
        %the end of the visible contour
        im_l(:,1)=1;
 im_r(:,end)=1;

        %to identify the interface ussing the diff method
        interface_l=zeros(size(im_l,2),1);
        interface_r=zeros(size(im_l,2),1);
            for iter=1:size(im_l,2)
                interface_l(iter)=max(find(diff(im_l(iter,:))~=0));%this is to consider the furthest pixel from the left
                interface_r(iter)=min(find(diff(im_r(iter,:))~=0));%this is to consider the furthest pixel form the right
            end
            
            contour_l_evolution=[contour_l_evolution  interface_l];
            contour_r_evolution=[contour_r_evolution  interface_r];
            
        end
        
        save(file4contour_evolution,'contour_l_evolution','contour_r_evolution');
        %clearvars contour_l_evolution contour_r_evolution
    end
end

                handles.status_text.String=['Detecting the leading edges of scratch ', num2str(s,'%g'),' in experiment ',num2str(exp,'%g'),...,
                    '      Progress: ', num2str(handles.num_scratches*(exp-1)+s,'%g'),'/ ',num2str(handles.num_scratches*handles.num_experiments,'%g'),...,
                    'of total scratches and ',num2str(k,'%g'),'/ ',num2str(num_images,'%g'),' of the images within this scratch'];
                
                drawnow;
                pause(1);