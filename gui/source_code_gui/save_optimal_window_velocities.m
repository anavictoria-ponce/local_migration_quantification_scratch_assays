%saves the optimal window velocities
%file part of the GUI to perform the quantification method
%----------------------------------------------------------------
%     Copyright (C) 2018 Ana Victoria Ponce Bobadilla
%     email: anavictoria.ponce@iwr.uni-heidelberg.de
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.
handles.status_text.String=strcat('Saving optimal window ');
     drawnow;
     pause(1);

    filename=strcat(handles.output_folder,'/optimal_window.mat');
    load(filename);
window_size=optimal_window;

%setting num_win_cells and window_size according to the optimal window size
%window_size=handles.optimal_window;
windowed_subdomain=window_size:window_size:handles.sizeofdomain;
            windowed_subdomain=[ 0 windowed_subdomain];
            if(windowed_subdomain(end)~=handles.sizeofdomain)
                windowed_subdomain=[windowed_subdomain handles.sizeofdomain ];
            end
            num_win_cells= length( windowed_subdomain)-1;

%saving the individual velocities
group_vel_index=[];

velocities4all=[];

  for s=1:handles.num_scratches
for exp=1:handles.num_experiments

   %     s=2;
        folder4_exp_s=strcat(handles.output_folder,handles.dir4_each_experiment,num2str(exp,'%g'),'/',num2str(s,'%g'),'/');
        file_windowed_velocities=strcat(folder4_exp_s,'windowed_velocities.mat');
        load(file_windowed_velocities);

         vels=[vel_l_w(window_size,1:num_win_cells) vel_r_w(window_size,1:num_win_cells)];

         velocities4all=[velocities4all vels'];

    end

end

velocity_groups_measurement=zeros(2*handles.num_experiments*handles.num_measurements_per_exp*num_win_cells,handles.num_groups);

%saving the group velocities
for group=1:handles.num_groups

    velocity_vec_per_group=[ ];

    for scratch=1+handles.num_measurements_per_exp*(group-1):group*handles.num_measurements_per_exp

        for exp=1:handles.num_experiments
            folder4_exp_s=strcat(handles.output_folder,handles.dir4_each_experiment,num2str(exp,'%g'),'/',num2str(scratch,'%g'),'/');
            file_windowed_velocities=strcat(folder4_exp_s,'windowed_velocities.mat');
            load(file_windowed_velocities);
            velocity_vec_per_group=[velocity_vec_per_group vel_l_w(window_size,1:num_win_cells)]; %to check that we are coding correctly the group samples

           velocity_vec_per_group=[velocity_vec_per_group vel_r_w(window_size,1:num_win_cells)]; %to check that we are coding correctly the group samples
            %   vec_test1=[vec_test1 scratch]
        end
        %  vec_test=[vec_test scratch]
    end
    %  area_groups(group,:)= area_vec';
   velocity_groups_measurement (:,group) =velocity_vec_per_group';
end

    filename=strcat(handles.output_folder,'/optimal_windowed_velocities.mat');
    save(filename,'velocities4all','optimal_window','num_win_cells','velocity_groups_measurement');
