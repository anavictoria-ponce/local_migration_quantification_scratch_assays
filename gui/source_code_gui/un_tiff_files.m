%un tiff the images
%file part of the GUI to perform the quantification method
%----------------------------------------------------------------
%     Copyright (C) 2018 Ana Victoria Ponce Bobadilla
%     email: anavictoria.ponce@iwr.uni-heidelberg.de
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.
flag=0;

for exp=1:handles.num_experiments
    
    number_pictures_4scratch=zeros(handles.num_scratches,1);
    
    for s=1:handles.num_scratches
        %  pause(.1); %// The pause is just so we see the numbers changing in the text box.
     
            file_in=[handles.exp_data_folder_text.String,handles.dir4_each_experiment,num2str(exp,'%g'),'/',...,
                num2str(s,'%03g'),' Transmitted no shutter.tif'];
        if (exist(file_in, 'file')==2)        
            info = imfinfo(file_in);
            number_pictures_4scratch(s)=numel(info);
            num_images=numel(info);
            folder_out=strcat(handles.output_folder_text.String,handles.dir4_each_experiment,num2str(exp,'%g'),'/',...,
                num2str(s,'%g'),'/');   
            
            
      
            for k = 1:num_images
                
                  handles.status_text.String=['Decompressing images of scratch ', num2str(s,'%g'),' in experiment ',num2str(exp,'%g'),...,
                      '      Progress: ', num2str(handles.num_scratches*(exp-1)+s-1,'%g'),'/ ',num2str(handles.num_scratches*handles.num_experiments,'%g'),...,
                      ' of total scratches and ',num2str(k,'%g'),'/ ',num2str(num_images,'%g'),' of the images within this scratch'];
            drawnow;
            
                %  k
                file_out=strcat(folder_out, handles.filename4_unsegmentedscratch,num2str(k,'%g'),'.png');
                tmp = imread(file_in, k);
                A(:,:,k)=(tmp(1:1:end,handles.sampling_factor:1:end));
                imwrite(A(:,:,k),file_out)
            end
        else
            string=['The file ',file_in,' does not exist. Is your experimental folder correct?'];
        f = errordlg(string);
        flag=1;
        break;
        end
    end
    
    if(flag==1)
          break;
    end
      
    filename2save_num=strcat(handles.output_folder,handles.dir4_each_experiment,num2str(exp,'%g'),'/number_pictures_4scratch.mat');
    save(  filename2save_num,'number_pictures_4scratch')
    
end


if(flag==0)
    
    handles.status_text.String=['Decompressing images of scratch ', num2str(s,'%g'),' in experiment ',num2str(exp,'%g'),...,
                      '     Progress: ', num2str(handles.num_scratches*(exp-1)+s,'%g'),'/ ',num2str(handles.num_scratches*handles.num_experiments,'%g'),...,
                      ' of total scratches and ',num2str(k,'%g'),'/ ',num2str(num_images,'%g'),' of the images within this scratch'];
    
            drawnow;
            pause(1);
            handles.status_text.String ='Done!';
drawnow;
pause(1);

end
          
            