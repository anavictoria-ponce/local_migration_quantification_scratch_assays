%normality tests between the group's velocities
%file part of the GUI to perform the quantification method
%----------------------------------------------------------------
%     Copyright (C) 2018 Ana Victoria Ponce Bobadilla
%     email: anavictoria.ponce@iwr.uni-heidelberg.de
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.
filename=strcat(handles.output_folder,'/optimal_windowed_velocities.mat');
if (exist(filename, 'file')==2)
   load(filename);
normality_tests_results=zeros(handles.num_groups,3);

   test2apply= get(handles.popupmenu_normality_test,'Value');
   if (test2apply==1)%Shapiro-Wilk test
       test_in_normality_function=7;
   elseif(test2apply==2)%Anderson-Darling test
              test_in_normality_function=5;
   elseif(test2apply==3)%  Kolmogorov-Smirnov test
        test_in_normality_function=1;
   elseif(test2apply==4)%  Liliefors
  test_in_normality_function=4;
   end



for i=1:handles.num_groups

ans=normalitytest(velocity_groups_measurement(:,i)');


normality_tests_results(i,1)=i;%group
normality_tests_results(i,2)=ans(test_in_normality_function,2);% p-value
normality_tests_results(i,3)=ans(test_in_normality_function,3);%test decision


    
end
handles.normality_tests_results=normality_tests_results;
t = handles.table_normality_test;
set(t,'Data',normality_tests_results); % Use the set command to change the uitable properties.

else
    string=['The velocities need to be calculated before.'];
    f = errordlg(string);
end

