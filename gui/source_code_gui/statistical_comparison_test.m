%performs the statistical comparison test
%file part of the GUI to perform the quantification method
%----------------------------------------------------------------
%     Copyright (C) 2018 Ana Victoria Ponce Bobadilla
%     email: anavictoria.ponce@iwr.uni-heidelberg.de
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.
sel_num=get(handles.statiscal_comparison_panel,'SelectedObject');
sel_string=get(sel_num,'String');%this defines the statistical comparison test that we are perfoming

%uploading the velocities
filename=strcat(handles.output_folder,'/optimal_windowed_velocities.mat');
if (exist(filename, 'file')==2)
load(filename);
data=velocity_groups_measurement;

    %test chosen to apply
    test2apply= get(handles.listbox_stat_test_comparison,'Value');
    
    
%test between two groups
if (length(sel_string)==11)
    
    %setting the two groups
    group1= get(handles.listbox_group2test,'Value');
    group2= get(handles.listbox_group2test_against,'Value');
    
    % vector to save stat  results
    stat_comparison_result=zeros(1,3);
    
    

    
    if (test2apply==1)%t test
        [stat_comparison_result(3),stat_comparison_result(2)]=ttest2(data(:,group1),data(:,group2));
    elseif(test2apply==2)% ks test
        [stat_comparison_result(3),stat_comparison_result(2)]=kstest2(data(:,group1),data(:,group2));
    elseif(test2apply==3)%rank sum test
        [stat_comparison_result(3),stat_comparison_result(2)]=ranksum(data(:,group1),data(:,group2));
    end
    
    t=handles.table_comparison_test;
    set(t,'Data', stat_comparison_result); % Use the set command to change the uitable properties.
    set(t,'ColumnName',{' ','p-value';'Test decision:'})
    set(t, 'ColumnWidth', {120, 100});
    
elseif (length(sel_string)==29)%one group against all the others
    
    
    %selected group
    group2test= get(handles.listbox_groupsallagainst,'Value');
    other_groups=1:handles.num_groups;
    other_groups=setdiff(other_groups,group2test);
    stat_comparison_result=zeros(length(other_groups),3);
    for i=1:length(other_groups)
        stat_comparison_result(i,1)=i;
        if (test2apply==1)%t test
            [stat_comparison_result(i,3),stat_comparison_result(i,2)]=ttest2(data(:,group2test),data(:,other_groups(i)));
        elseif(test2apply==2)% ks test
            [stat_comparison_result(i,3),stat_comparison_result(i,2)]=kstest2(data(:,group2test),data(:,other_groups(i)));
        elseif(test2apply==3)%rank sum test
            [stat_comparison_result(i,3),stat_comparison_result(i,2)]=ranksum(data(:,group2test),data(:,other_groups(i)));
        end
    end
    
    t=handles.table_comparison_test;
    set(t,'Data', stat_comparison_result); % Use the set command to change the uitable properties.
    t.ColumnName ={'Group:','p-value','Test decision:'};
    set(t, 'ColumnWidth', {50,120,100});
    
end

handles.stat_comparison_result=stat_comparison_result;

else
    string=['The velocities need to be calculated before.'];
    f = errordlg(string);
end

