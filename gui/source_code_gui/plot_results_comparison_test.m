%output the results of the statistical tests
%file part of the GUI to perform the quantification method
%----------------------------------------------------------------
%     Copyright (C) 2018 Ana Victoria Ponce Bobadilla
%     email: anavictoria.ponce@iwr.uni-heidelberg.de
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.
%letting the user define the file which will save the comparison test
%results
[file,path] = uiputfile('comparison_test_results.txt');
if (file~=0)
filename=[path,file];
fileID = fopen(filename,'w');

%read the statistical test that was compared
sel_num=get(handles.statiscal_comparison_panel,'SelectedObject');
sel_string=get(sel_num,'String');%this defines the statistical comparison test that we are perfoming

%test chosen to apply
test2apply= get(handles.listbox_stat_test_comparison,'Value');

if (test2apply==1)%t test
    test_string=' t-test';
elseif(test2apply==2)% ks test
    test_string=' Kolmogorov Smirnov test';
elseif(test2apply==3)%rank sum test
    test_string=' Wilcoxon rank sum test';
end

initial_string='Comparison test using the ';

%test between two groups
if (length(sel_string)==11)% one group against the other one
        
    
    group1= get(handles.listbox_group2test,'Value');
    group2= get(handles.listbox_group2test_against,'Value');
    title_string=strcat(initial_string,test_string,' between the groups',num2str(group1,'%g'),' and ',num2str(group2,'%g'),' velocities \n');
    fprintf(fileID,title_string);
    fprintf(fileID,'p-vale:     Test decision:\n');
    fprintf(fileID,'%6.4f     %g\n',handles.stat_comparison_result(1),handles.stat_comparison_result(2));
    
    fclose(fileID);
    
    
elseif (length(sel_string)==29)%one group against all the others
    
    group2test= get(handles.listbox_groupsallagainst,'Value');
    other_groups=1:handles.num_groups;
    other_groups=setdiff(other_groups,group2test);
    title_string=strcat(initial_string,test_string,' between group ',num2str(group2test,'%g'),' against all the other groups'' velocities \n');
    fprintf(fileID,title_string);
    fprintf(fileID,'Group:    p-vale:   Test decision:\n');
    
    for i=1:length(other_groups)
        
        fprintf(fileID,'%g        %6.4f     %g\n',handles.stat_comparison_result(i,1),handles.stat_comparison_result(i,2),handles.stat_comparison_result(i,3));
    end
        
    fclose(fileID);
end

else
    string=['No filename has been entered. The results of the comparison test have not been saved'];
    f = errordlg(string);
end

