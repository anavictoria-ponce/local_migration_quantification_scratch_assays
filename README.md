This repository contains the source code and the GUI for the
quantification method described in the manuscript:
"In vitro cell migration quantification method for scratch assays".
---
The in silico and in vitro data is available at:
<https://ganymed.math.uni-heidelberg.de/~victoria/supplementary_data_migration_quantification_scratch_assays.shtml>

A test data set and the instructions on how the GUI is executed can be found in the GUI folder.

The source code of the analysis performed in the manuscript can be found in the source_code folder.
